#!/usr/bin/env python3
import argparse
import configparser
import datetime
import json
import locale
import pathlib
import socket
import sys
import urllib
import urllib.request

import rrdtool

CONFIG_VERSION = 3

MAX_TEMPERATURE_DIFF = 2.0
MAX_PRESSURE_DIFF = 35.0
MAX_HUMIDITY_DIFF = 10.0

WEATHER_URI = "https://api.openweathermap.org/data/2.5/weather?lat=%s&lon=%s&units=metric&appid=%s"


class RemoteTemp:
    def __init__(self, configfile):
        config = configparser.RawConfigParser()
        self.minimum_min_delay = 15
        self.latitude = 0
        self.longitude = 0
        self.api_key = ""
        self.temperature_file = ""
        self.rrd_database_file = ""
        if configfile:
            config.read_file(configfile)
            if not config.has_option("global", "version"):
                print("version in config file missing.")
                sys.exit(254)
            version = config.getint("global", "version")
            if version != CONFIG_VERSION:
                print(f"App needs config version '{CONFIG_VERSION}' but config file is of version '{version}'")
                sys.exit(255)
            if config.has_option("temp_remote", "minimum_min_delay"):
                self.minimum_min_delay = config.getint("temp_remote", "minimum_min_delay")
            if config.has_option("temp_remote", "latitude"):
                self.latitude = config.get("temp_remote", "latitude")
            if config.has_option("temp_remote", "longitude"):
                self.longitude = config.get("temp_remote", "longitude")
            if config.has_option("temp_remote", "api_key"):
                self.api_key = config.get("temp_remote", "api_key")
            if config.has_option("temp_remote", "temperature_file"):
                self.temperature_file = config.get("temp_remote", "temperature_file")
            if config.has_option("global", "rrd_database_file"):
                self.rrd_database_file = config.get("global", "rrd_database_file")

        self.remote_place = ""
        self.remote_temp = "U"

    def __parse_response(self, response):
        try:
            if isinstance(response, list):
                response = response[0]
            try:
                data = json.loads(response)
                if data:
                    if 'main' in data and data['main']:
                        main_section = data['main']
                        if 'temp' in main_section and main_section['temp']:
                            self.remote_temp = f"{main_section['temp']:.1f}"
                    if 'name' in data and data['name']:
                        self.remote_place = data['name']
            except ValueError:
                print("parsing of json result failed!")
        except IOError:
            print("connection to <%s> failed", WEATHER_URI)

    def get_uri(self):
        url = WEATHER_URI % (self.latitude, self.longitude, self.api_key)
        try:
            with urllib.request.urlopen(url) as result:
                charset = result.info().get_content_charset()
                content = result.read().decode(charset)
                content = content.split('\n')
                for response in content:
                    self.__parse_response(response)
        except socket.error:
            print(f"connection to <{url}> failed.")

    def get_remote_temp(self):
        cur_datetime = datetime.datetime.now()

        cur_datetime += datetime.timedelta(seconds=30)
        cur_datetime = cur_datetime.replace(second=0, microsecond=0)

        file = pathlib.Path(self.temperature_file)
        try:
            line = file.read_text(encoding=locale.getencoding())
            date_str = line.split(';')[0].strip()
            temperature_remote = line.split(';')[-1].strip()

            last_update = datetime.datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")
            date_diff = cur_datetime - last_update
            if date_diff < datetime.timedelta(minutes=self.minimum_min_delay):
                return temperature_remote
        except (FileNotFoundError, ValueError):
            pass

        self.get_uri()
        if self.remote_temp != "U":
            cur_datetime_str = cur_datetime.strftime("%F %T")
            #  print(f"date: '{cur_datetime_str}'; temp: '{temperature_remote}'")
            file.write_text(f"{cur_datetime_str}; {self.remote_place}; {self.remote_temp}\n",
                            encoding=locale.getencoding())

        return self.remote_temp


def get_tph_data():
    try:
        import sense_hat  # pylint: disable=C0415

        sense = sense_hat.SenseHat()
        temperature = sense.get_temperature()
        pressure = sense.get_pressure()
        humidity = sense.get_humidity()
        return temperature, pressure, humidity
    except (ImportError, OSError, IOError):
        pass

    try:
        import smbus2  # pylint: disable=C0415
        import bme280  # pylint: disable=C0415
        port = 1
        address = 0x76
        bus = smbus2.SMBus(port)

        calibration_params = bme280.load_calibration_params(bus, address)
        data = bme280.sample(bus, address, calibration_params)
        return data.temperature, data.pressure, data.humidity
    except (ImportError, OSError, IOError):
        pass
    return "U", "U", "U"


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c",
                        "--configfile",
                        help="name of configfile",
                        type=argparse.FileType(),
                        required=True
                        )
    args = parser.parse_args()

    return args


def main():
    args = parse_args()

    temperature, pressure, humidity = get_tph_data()

    remotetemp = RemoteTemp(args.configfile)
    temperature_remote = remotetemp.get_remote_temp()

    # pylint: disable=I1101
    rrdtool.update(remotetemp.rrd_database_file, f"N:{temperature}:{pressure}:{humidity}:{temperature_remote}")


if __name__ == "__main__":
    main()
