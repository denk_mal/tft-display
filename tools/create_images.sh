#!/bin/bash
#
# create images from data collected by external temp script
SCRIPT_FULLPATH=$(readlink -f $0)
SCRIPT_BASE_DIR=${SCRIPT_FULLPATH%/*}

BASE_DIR="/opt/rrd"
IMAGE_BASE_DIR="/var/www/html/images/rrd"
DB_FILE="database.rrd"
TITLE1="Flat Painter"
MIN_TEMP_THRESHOLD="10"
MAX_TEMP_THRESHOLD="22"
MIN_TEMP="10"
MAX_TEMP="30"
MIN_PRESS="950"
MAX_PRESS="1050"
MIN_PRESS_THRESHOLD="990"
MAX_PRESS_THRESHOLD="1035"
MIN_HUMID="0"
MAX_HUMID="100"
MIN_HUMID_THRESHOLD="35"
MAX_HUMID_THRESHOLD="65"
MIN_TEMP_REMOTE_THRESHOLD="3"
MAX_TEMP_REMOTE_THRESHOLD="21"
MIN_TEMP_REMOTE="-10"
MAX_TEMP_REMOTE="30"

declare -A PARAMS=( [3h]=-10800 [36h]=-129600 [woc]=-604800 [month]=-2851200 [year]=-31622400 )

# overwrite preset in a local settingsfile
test -s $SCRIPT_BASE_DIR/defines.local && . $SCRIPT_BASE_DIR/defines.local

if [ ! -d $IMAGE_BASE_DIR ]; then
  mkdir -m 775 -p $IMAGE_BASE_DIR
fi
chmod a+w $IMAGE_BASE_DIR

function createPng() {
  local params=("${!1}")
  local TITLE=${params[0]}
  local VERT_TITEL=${params[1]}
  local TARGET_FILE=${params[2]}
  local START_TIME=${params[3]}
  if [ $START_TIME -gt 0 ]; then
    START_TIME="-$START_TIME"
  fi
  local SOURCE=${params[4]}
  local VALUE_LOW=${params[5]}
  local VALUE_HIGH=${params[6]}
  local THRESHOLD_LOW=${params[7]}
  local THRESHOLD_HIGH=${params[8]}
  local SCALING=${params[9]}

  local display_strings=("${!2}")

  /usr/bin/rrdtool graph $IMAGE_BASE_DIR/$TARGET_FILE.png \
  --width 600 \
  --height 200 \
  --imgformat PNG \
  --lower-limit $VALUE_LOW \
  --upper-limit $VALUE_HIGH \
  --title "$TITLE" \
  --start $START_TIME -A \
  --vertical-label "$VERT_TITEL" \
  --units-exponent 0 \
  --y-grid=$SCALING \
  --slope-mode \
  --watermark "`date`" \
  DEF:g1=$BASE_DIR/$DB_FILE:$SOURCE:AVERAGE \
  DEF:gmin=$BASE_DIR/$DB_FILE:$SOURCE:MIN \
  DEF:gmax=$BASE_DIR/$DB_FILE:$SOURCE:MAX \
  VDEF:g1a=g1,LAST \
  VDEF:gmina=gmin,MINIMUM \
  VDEF:gmaxa=gmax,MAXIMUM \
  CDEF:blau=g1,$THRESHOLD_LOW,LT,g1,UNKN,IF \
  CDEF:rot=g1,$THRESHOLD_HIGH,GT,g1,UNKN,IF \
  CDEF:gzw=g1,$THRESHOLD_HIGH,1,+,LT,g1,UNKN,IF \
  CDEF:gruen=gzw,$THRESHOLD_LOW,GT,gzw,UNKN,IF \
  LINE1:blau#0000ff:"${display_strings[0]}\l" \
  COMMENT:"\u" \
  LINE1:gruen#00ff00:"${display_strings[1]}\c" \
  COMMENT:"\u" \
  LINE1:rot#ff0000:"${display_strings[2]}\r" \
  GPRINT:gmina:"${display_strings[3]}\l" \
  COMMENT:"\u" \
  GPRINT:gmaxa:"${display_strings[4]}\c" \
  COMMENT:"\u" \
  GPRINT:g1a:"${display_strings[5]}\r" \
  > /dev/null
}

function createPngTemp() {
  local param_array=("Temperatur" "Grad Celsius" "$1" "$2" "$3" "$MIN_TEMP" "$MAX_TEMP" "$MIN_TEMP_THRESHOLD" "$MAX_TEMP_THRESHOLD" "5:2")
  TEMP_STRINGS=(
    "Temperatur unter $MIN_TEMP_THRESHOLD °C,"
    "zwischen $MIN_TEMP_THRESHOLD und $MAX_TEMP_THRESHOLD °C,"
    "Temperatur über $MAX_TEMP_THRESHOLD °C"
    "Tiefsttemperatur\: %5.2lf °C"
    "Höchsttemperatur\: %5.2lf °C"
    "aktuelle Temperatur\: %5.2lf °C"
  )

  createPng param_array[@] TEMP_STRINGS[@]
}

function createPngPressure() {
  local param_array=("Luftdruck" "hPa" "$1" "$2" "$3" "$MIN_PRESS" "$MAX_PRESS" "$MIN_PRESS_THRESHOLD" "$MAX_PRESS_THRESHOLD" "5:2")
  TEMP_STRINGS=(
    "Luftdruck unter $MIN_PRESS_THRESHOLD hPa,"
    "zwischen $MIN_PRESS_THRESHOLD und $MAX_PRESS_THRESHOLD hPa,"
    "Luftdruck über $MAX_PRESS_THRESHOLD hPa"
    "Tiefstdruck\: %5.2lf hPa"
    "Höchstdruck\: %5.2lf hPa"
    "aktueller Luftdruck\: %5.2lf hPa"
  )

  createPng param_array[@] TEMP_STRINGS[@]
}

function createPngHumidity() {
  local param_array=("Luftfeuchte" "%" "$1" "$2" "$3" "$MIN_HUMID" "$MAX_HUMID" "$MIN_HUMID_THRESHOLD" "$MAX_HUMID_THRESHOLD" "5:2")
  TEMP_STRINGS=(
    "Luftfeuchte unter $MIN_HUMID_THRESHOLD %,"
    "zwischen $MIN_HUMID_THRESHOLD und $MAX_HUMID_THRESHOLD %,"
    "Luftfeuchte über $MAX_HUMID_THRESHOLD %"
    "Min. Luftfeuchte\: %5.2lf %%"
    "Max. Luftfeuchte\: %5.2lf %%"
    "aktuelle Luftfeuchte\: %5.2lf %%"
  )

  createPng param_array[@] TEMP_STRINGS[@]
}

function createPngTempRemote() {
  local param_array=("Temperatur Außen" "Grad Celsius" "$1" "$2" "$3" "$MIN_TEMP_REMOTE" "$MAX_TEMP_REMOTE" "$MIN_TEMP_REMOTE_THRESHOLD" "$MAX_TEMP_REMOTE_THRESHOLD" "5:2")
  TEMP_STRINGS=(
    "Temperatur unter $MIN_TEMP_REMOTE_THRESHOLD °C,"
    "zwischen $MIN_TEMP_REMOTE_THRESHOLD und $MAX_TEMP_REMOTE_THRESHOLD °C,"
    "Temperatur über $MAX_TEMP_REMOTE_THRESHOLD °C"
    "Tiefsttemperatur\: %5.2lf °C"
    "Höchsttemperatur\: %5.2lf °C"
    "aktuelle Temperatur\: %5.2lf °C"
  )

  createPng param_array[@] TEMP_STRINGS[@]
}

for key in "${!PARAMS[@]}"; do
  createPngTemp "temp1_$key" ${PARAMS[$key]} temperature
  createPngPressure "press1_$key" ${PARAMS[$key]} pressure
  createPngHumidity "humid1_$key" ${PARAMS[$key]} humidity
  createPngTempRemote "temp_remote1_$key" ${PARAMS[$key]} temperature_remote
done
