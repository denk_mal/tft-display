# tft-display
A pygame based, modularised display app for raspberry pi tft displays.

The goal of this Project is to have a base application, that could be extended by module
that serve a special, minimal function e.g. get the temperature from a sensor and display
it on the screen. The application could be extended by own modules without any modifications
of the main application. There were loaded dynamicly on the startup.

There are 2 possible kinds of modules; Draw modules that are working on a time sliced base
(every second, every minute and every hour) and Event modules that are responding of an
(external) event.

The design of the modules (position, size, color, framedrawing, textalignment) is completely
controlled by the settings in the config.ini file.

## Draw Modules
A draw module (source code filename starts with draw) must fetch they own data and draw the
display box. To make it easier two base classes exists; The BaseDrawClass for simplifing
drawing of frames and core things and the BaseTextboxClass for a simple display of text.

#### drawClock
A simple analog clock with seconds hand

#### drawDate
A single Textline containing the date

#### drawTemp
shows the temperature from the Pi Sense HAT or from a 1-wire device

#### drawHumidity
shows the humidity from the Pi Sense HAT

#### drawPressure
shows the pressure from the Pi Sense HAT; uses a dynamic textbox to display minimum and
maximum pressure for the last hour

#### drawTempRemote
gets the temperature from a website and displays it; Uses a dynamic Textbox for displaying
sunrise and sunset.

#### drawSysteminfo
shows some network settings (ip-address, hostname, uptime)

#### drawPicture
a simple picture viewer module

## Event Modules
An event module (source code file starts with event) must start a separate Thread that
will silently work in the background and can send out one of two events:

An alert event will stop the displaying of any other data and shows a dialog box with
the alertstring. **For now the event module must reset the alert state.** Alert states are
not stackable.

A dynamic text event will update the content of a dynamic textbox.

#### eventFritzbox
controls the Fritzbox call monitor telnet interface. It initiates an alert by an incoming call
and fills three dynamic textboxes for incoming calls, outgoing calls and missing calls. The last
10 entries of each list will be saved if a filename is specified.

If you export the phonebook from the fritzbox then it could be used as a number-name translation
for phonenumber that are saved in the phonebook.
A phonenumber normalizer function is able to compensate phonenumbers that different only in country
and/or region prefixes if the correct country_code and area_code is set in the config.ini file

E.g. with country_code = 49  and area_code = 30 the phonenumber 12345678 will be equal to 03012345678
or 00493012345678 (or +493012345678)

If 'fritzconnection' is installed than it is possible to setup this module for automatic phonebook download.
For using this feature, you either need to install `pip install fritzconnection` or download 
[fritzconnection.py](https://bitbucket.org/denkmal/fritzconnection/src) to the modules folder. The later one
could using a ssl connection to the fritzbox and has a better handling of wrong account settings.

**Please keep in mind that the password in the config file is only base64 encoded; this is not very secure!**

## Dynamic Boxes
Dynamic boxes were create "on the fly". That means that they are not visible before the first use.
If the definition is missing in the config file than the dynamic box is ignored.

#### Dynamic Textboxes
The content of those textbox will be filled from the data of the dynamic textbox event.

#### Dynamic Drawboxes
The content of those drawboxes are one of four(five) simple objects (square, triangle up, triangle down
and rectangle). By setting the color to the background color it is possible to get an "empty" box.

## Configuring the modules
The config file (normally config.ini) contains a lot of settings and is mandatory.
The mainobject is the modules dictionary. Every draw module that should be used must be inserted here.
All modules that aren't placed in this dict would not be initialised and could therefore not be used.
If the draw areas are overlapping than the result is undefined so keep attention about non-overlapping
draw areas. The source for a module <name> is the script in the modules folder named draw<name>.py; It
must contain a class <Name> (notice: uppercase/lowercae is important).
If you wish to write your own module keep looking at the 'Modules API' section below.

E.g a new modules for displaying pictures that would defined in the modules dict as 'pictureView' must
reside in the sourcefile modules/drawPictureView.py and must contain a class named 'DrawPictureView'.
And in the opposide every sourcefile in modules with the prefix 'draw' that contains a class with the prefix
'Draw' could be used as module in the modules dictionary.

For further details about detailed settings see the  config.ini.template file.
Mandatory entries are marked as MANDATORY

## Module API
To implement your own module you have to create a new class that has the following methods:
```python
def __init__(self, config, event_types, x1_y1, x2_y2, inner_offset, frame, h_align, v_align):
```
This is the constructor method called by python on object creation.

```python
def post_init(self):
```
This method is called after _**all**_ modules and eventhandlers have been loaded and the event handling queue
is ready for receiving events. This is the function that should send out events for drawing dynamic boxes for
the first time and where background processes should be started.

```python
def draw(self):
```
This is the main draw method that handles all neccessary drawings including cleanup of the draw area.

If one of the modules has exclusive access than these method will only be called for the active module; All modules
that haven't exclusive access will not be called!

```python
def click_event(self, click_pos, box_name):
```
This is the Click-Event handling function that will be called when a Mouse Down event occured (pygame.MOUSEBUTTONDOWN)
inside of the drawarea of the mainobject or in the drawarea of dynamic boxes owned by this module. The _click_pos_ 
paramater contains the click pos relative to the top left of the box; the _box_name_ parameter contains the name of
the dynamic box that has received the click or an empty string if the click event was received by the main object.

```python
def every_sec(self):
def every_min(self):
def every_hour(self):
```
Theses three methods are drawmodule specific and will be called on a regular base (as the name already suggested)
The every_sec() function must return 'TRUE' if an immediately redraw of all objects is necessary. After the call of
every_min() (and of every_hour()) a redraw is done automatically.

For easier writing of code a BaseDrawClass file exists that will handle all basic things that are necessary for simple
draw actions and core handling of drawmodules. A second base class (BaseTextboxClass) allow easier use of text based
content.

### Getting exclusive access
To get exclusive access to the display and avoid draw updates from other events the module has to send out an event to
the main event loop to request exclusive access. If it is ok that the module can get exclusive access a method of the
module will be called. As long as the app is in exclusive access state only the draw() and every_xxx() method of the 
current object will be called. The draw() and every_xxx() methods of all other
 **It is up to the module to release exclusive access!**

The event for exclusive start/stop has two parameters:
- block: set to "--YES--" to start exclusive access; any other content will stop exclusive access
- receiver: module that contains the acknowlage method that will be called for starting exclusive access

```python
def exclusive_access(self):
```
This method is the starting point for exclusive draw access. 

**Note:** it is possible to end exclusive from any module but while the module that starts exclusive access will not be
notified draw clutter could (and would) happen!

## TODOs and possible Features
- [x] implement exclusive access handling to the complete drawing area
- [x] remove python 2.x support
- [ ] use api wrapper for access openweathermap.org and worldweatheronline.com api's in TempRemote
- [ ] startmode (normal interface/exclusive access of one module)
- [ ] dynamic toggle of normal interface and/or exclusive access of modules
