#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging

import base64
import datetime
import os
import queue
import re
import requests
import threading
import time
from xml.etree import ElementTree
import urllib.request

import fritzconnection.core.utils
import pygame

from fritzconnection import FritzConnection
from fritzconnection import FritzMonitor
from fritzconnection.lib.fritzcall import FritzCall
from fritzconnection.lib.fritzstatus import FritzStatus

from .base_textbox_class import BaseTextboxClass

FRITZBOX_HOST = "fritz.box"
FRITZBOX_PORT = 1012

COUNTRY_CODE = "+49"
AREA_CODE = "40"


class EventFritzbox(BaseTextboxClass):
    # pylint: disable=too-many-instance-attributes
    def __init__(self, config, event_types):
        maxlistsize = config.getint('maxlistsize', 10, 'fritzbox')
        screen_x = config.getint('size_x', 320)
        screen_y = config.getint('size_y', 240)
        super().__init__(config, event_types, (0, 0), (screen_x, screen_y), 20, 0b1111, 0, 0,
                         maxlistsize + 2, True)
        self._event_types = event_types
        self.__config = config
        self.__fb_host = config.get('host', FRITZBOX_HOST, self.classname)
        self.__fb_port = config.getint('port', FRITZBOX_PORT, self.classname)
        self.__fb_user = config.get('user', '', self.classname)
        hashvalue = config.get('hash', '', self.classname) + "==="
        hash_len = len(hashvalue) // 4
        self.__fb_pw = base64.b64decode(hashvalue[:hash_len * 4]).decode('utf-8')
        self.__countrycode = config.get('country_code', COUNTRY_CODE, self.classname)
        self.__areacode = config.get('area_code', AREA_CODE, self.classname)
        self.__maxlistsize = maxlistsize
        self.__showheader = config.getboolean('showheader', True, self.classname)

        self.__fritz_connection = None
        self.__fritz_connection_started = False
        self.__delta_sec = 0
        self.__connection_up = False

        # read telephonebook from fritzbox xml export
        self.phonebook = {}
        self.__init_telephonebook(True)

        # read calllists from fritzbox xml export
        self.__incoming_calls = []
        self.__outgoing_calls = []
        self.__missed_calls = []
        self.__init_calllists()

    def post_init(self):
        self.__send_event('fb_incoming_call')
        self.__send_event('fb_outgoing_call')
        self.__send_event('fb_missed_call')
        self.__set_status_state(-1)

        # start thread after sending out all init events
        t_worker = threading.Thread(target=self.__workerprocess)
        t_worker.daemon = True
        t_worker.start()

    def __open_connection(self):
        try:
            logging.info("open connection to fritzbox...")
            self.__fritz_connection_started = True
            self.__fritz_connection = FritzConnection(address=self.__fb_host, user=self.__fb_user,
                                                      password=self.__fb_pw, use_tls=True, timeout=10)
            if self.__fritz_connection:
                self.force_reload()
                self.__fritz_connection_started = False
        except fritzconnection.core.utils.FritzConnectionException:
            logging.error("connection to fritzbox failed!")
            self.__fritz_connection = None
            self.__fritz_connection_started = False

    def __get_connection(self):
        if not self.__fritz_connection and not self.__fritz_connection_started:
            t_worker = threading.Thread(target=self.__open_connection)
            t_worker.daemon = True
            t_worker.start()

        return self.__fritz_connection

    def normalize_telno(self, telno, replace_anonymous=True):
        chk = re.compile(r"^\+?[0-9]+$")
        if not chk.match(telno):
            if replace_anonymous:
                return self.__config.get('anonymous', '', self.classname)
            return telno
        if telno.startswith('00'):
            telno = f"+{telno[2:]}"
        elif telno.startswith('0'):
            telno = f"{self.__countrycode}{telno[1:]}"
        elif telno.startswith('+'):
            pass
        else:
            telno = f"{self.__countrycode}{self.__areacode}{telno}"
        return telno

    def __init_calllists(self):
        self.__incoming_calls = []
        self.__outgoing_calls = []
        self.__missed_calls = []

        content_missed, content_out, content_received = self.__get_calllist_from_fb()
        for contact in content_received:
            if contact.Name:
                infoline = f"{contact.Date} {contact.Name}"
            else:
                infoline = f"{contact.Date} {contact.Caller}"
            self.__incoming_calls.append(infoline)
            if len(self.__incoming_calls) >= self.__maxlistsize:
                break

        for contact in content_missed:
            if contact.Name:
                infoline = f"{contact.Date} {contact.Name}"
            else:
                infoline = f"{contact.Date} {contact.Caller}"
            self.__missed_calls.append(infoline)
            if len(self.__missed_calls) >= self.__maxlistsize:
                break

        for contact in content_out:
            if contact.Name:
                infoline = f"{contact.Date} {contact.Name}"
            else:
                infoline = f"{contact.Date} {contact.Called}"
            self.__outgoing_calls.append(infoline)
            if len(self.__outgoing_calls) >= self.__maxlistsize:
                break

    def __init_telephonebook(self, force_load):
        phonebook_path = self.__config.get("phonebook", "", self.classname)
        phonebook_path = os.path.expanduser(phonebook_path)
        phonebook_path = os.path.realpath(phonebook_path)
        try:
            new_phonebook = self.__get_phonebook_from_fb()
            if not force_load and not new_phonebook:
                return False
            self.phonebook = {}
            root = ElementTree.parse(phonebook_path)
            if not root:
                return False
            for contact in root.iter("contact"):
                person = contact.find("person")
                realname = person.find("realName")
                numbers = list(contact.iter("number"))
                for number in numbers:
                    if number.text:
                        nr_text = number.text
                        if nr_text[-1] == "*" and nr_text[0] != "#":
                            nr_text = nr_text[0:-1]
                        self.phonebook[self.normalize_telno(nr_text, False)] = realname.text
            return True
        except IOError as err:
            logging.error("could not open phonebook '%s'", phonebook_path)
            logging.error(err)
        return False

    def __get_calllist_from_fb(self):
        logging.info("get calllists from fritzbox!")
        con = self.__get_connection()
        if not con:
            return [], [], []
        try:
            call_con = FritzCall(con)
            content_missed = call_con.get_missed_calls()
            content_out = call_con.get_out_calls()
            content_received = call_con.get_received_calls()

            return content_missed, content_out, content_received
        except (ConnectionError, requests.ReadTimeout):
            logging.warning("connection failed in __get_calllist_from_fb()")
            self.__fritz_connection = None

        return [], [], []

    def __get_phonebook_from_fb(self):
        phonebook_path = self.__config.get("phonebook", "", self.classname)
        phonebook_path = os.path.expanduser(phonebook_path)
        phonebook_path = os.path.realpath(phonebook_path)
        refresh_days = self.__config.getint("refresh_days", 7, self.classname)
        days_ago = datetime.datetime.now() - datetime.timedelta(days=refresh_days)
        filetime = days_ago - datetime.timedelta(days=refresh_days + 1)
        try:
            filetime = datetime.datetime.fromtimestamp(os.path.getctime(phonebook_path))
        except IOError:
            pass

        if filetime < days_ago:
            logging.debug("try to get new phonebook...")
            logging.info("get phonebook from fritzbox!")
            con = self.__get_connection()
            if not con:
                return True
            try:
                result = con.call_action('X_AVM-DE_OnTel', 'GetPhonebook', NewPhonebookId=0)
                download_url = result['NewPhonebookURL']
                uri_parts = download_url.split(':')
                uri_parts[1] = "//" + self.__fb_host
                download_url = ":".join(uri_parts)
                logging.debug("Download URL: %s", download_url)
                urllib.request.urlretrieve(download_url, phonebook_path)
                return True
            except (ConnectionError, requests.ReadTimeout):
                logging.warning("connection failed in __get_phonebook_from_fb()")
                self.__fritz_connection = None
        else:
            logging.debug("filedate: %s; compare date: %s", filetime, days_ago)

        return False

    def __set_status_state(self, new_state):
        type_fail = '_REMOVE_'
        type_ok = '_REMOVE_'
        type_wait = '_REMOVE_'
        if new_state == 0:
            type_fail = 'drawbox'
        elif new_state > 0:
            type_ok = 'drawbox'
        else:
            type_wait = 'drawbox'
        pygame.event.post(pygame.event.Event(self._event_types[1], target_type=type_wait,
                                             id='fb_status_wait', receiver=self))
        pygame.event.post(pygame.event.Event(self._event_types[1], target_type=type_ok,
                                             id='fb_status_ok', receiver=self))
        pygame.event.post(pygame.event.Event(self._event_types[1], target_type=type_fail,
                                             id='fb_status_fail', receiver=self))

        if new_state < 1:
            self.__set_line_state(-1)

    def __set_line_state(self, new_state):
        type_free = '_REMOVE_'
        type_busy = '_REMOVE_'
        type_wait = '_REMOVE_'
        if new_state == 0:
            type_free = 'drawbox'
        elif new_state > 0:
            type_busy = 'drawbox'
        else:
            type_wait = 'drawbox'
        pygame.event.post(pygame.event.Event(self._event_types[1], target_type=type_free,
                                             id='fb_line_free', receiver=self))
        pygame.event.post(pygame.event.Event(self._event_types[1], target_type=type_busy,
                                             id='fb_line_busy', receiver=self))
        pygame.event.post(pygame.event.Event(self._event_types[1], target_type=type_wait,
                                             id='fb_line_unknown', receiver=self))

    # pylint: disable=too-many-locals
    def __prepare_calllist(self, event_id):
        headers = self.__config.getdict("headers", {}, self.classname)
        if event_id in headers:
            box_header = headers[event_id]
        else:
            box_header = ''

        if event_id == 'fb_incoming_call':
            return self.__incoming_calls, box_header
        if event_id == 'fb_outgoing_call':
            return self.__outgoing_calls, box_header
        if event_id == 'fb_missed_call':
            return self.__missed_calls, box_header
        return [], ''

    def __send_event(self, event_id):
        datalist, box_header = self.__prepare_calllist(event_id)
        if not self.__showheader:
            box_header = ''
        pygame.event.post(pygame.event.Event(self._event_types[1], target_type='textbox', id=event_id, data=datalist,
                                             receiver=self, header=box_header))

    # pylint: disable=too-many-nested-blocks
    def __workerprocess(self):
        while True:
            try:
                logging.info("try to connect fritzbox callmonitor....")
                # as a context manager FritzMonitor will shut down the monitor thread
                with FritzMonitor(address=self.__fb_host, port=self.__fb_port) as monitor:
                    logging.info("fritzbox callmonitor connection ok.")
                    self.__set_status_state(1)
                    self.__set_line_state(0)
                    event_queue = monitor.start()
                    self.__process_events(monitor, event_queue)
            except OSError:
                pass
            finally:
                self.__set_status_state(0)
                time.sleep(60)

    def __process_events(self, monitor, event_queue, healthcheck_interval=10):
        last_action_ring = False
        while True:
            try:
                event = event_queue.get(timeout=healthcheck_interval)
            except queue.Empty:
                # check health:
                if not monitor.is_alive or not self.__connection_up:
                    logging.warning("Connection closed by remote host.")
                    return
            else:
                edata = event.split(';')
                line_is_busy = 1
                message = ""
                if edata[1] == "RING":
                    last_action_ring = True
                    call_nr = self.normalize_telno(edata[3])
                    call_name = ""

                    for index, _unused in enumerate(call_nr):
                        call_nr_tmp = call_nr[0:len(call_nr) - index]
                        if call_nr_tmp in self.phonebook:
                            call_name_tmp = self.phonebook[call_nr[0:len(call_nr) - index]]
                            call_name_tmp += call_nr[len(call_nr_tmp):]
                            call_name = f"{call_name_tmp:25}"
                            break
                    if not call_name:
                        call_name = call_nr
                        call_nr = ""

                    if call_nr:
                        message = f"Eingehender Anruf\nvon: {call_name}\n({call_nr})\nan: {edata[4]}"
                    else:
                        message = f"Eingehender Anruf\nvon: {call_name}\nan: {edata[4]}"

                    logging.info("Incoming call from: %s to: %s", edata[3], edata[4])
                elif edata[1] == "CALL":
                    logging.info("Outgoing call from: %s to: %s", edata[4], edata[5])
                elif edata[1] == "CONNECT":
                    logging.info("Call accepted")
                    last_action_ring = False
                elif edata[1] == "DISCONNECT":
                    line_is_busy = 0
                    if last_action_ring:
                        logging.info("Call missed")
                    else:
                        logging.info("Call ended")
                    last_action_ring = False

                self.__set_line_state(line_is_busy)
                if message:
                    # show alert
                    pygame.event.post(pygame.event.Event(self._event_types[0], text=message))
                else:
                    # clear alert
                    pygame.event.post(pygame.event.Event(self._event_types[0], {}))
                if line_is_busy < 1:
                    self.__init_calllists()
                    self.__send_event('fb_incoming_call')
                    self.__send_event('fb_outgoing_call')
                    self.__send_event('fb_missed_call')

    def click_event(self, click_pos, box_name):
        logging.debug("click event received in '%s'", type(self).__name__)
        if box_name.startswith('fb_status_'):
            self.__init_telephonebook(False)
            self.__send_event('fb_incoming_call')
            self.__send_event('fb_outgoing_call')
            self.__send_event('fb_missed_call')
            return
        if box_name.startswith('fb_line_'):
            return
        if box_name == 'exclusive_access':
            pass
        else:
            datalist, box_header = self.__prepare_calllist(box_name)
            if not box_header:
                box_header = box_name

            self.set_header([box_header, '================================='])
            self.set_content(datalist)
        super().click_event(click_pos, box_name)

    def _drawframe(self):
        window = self._config.getwindow()
        pygame.draw.rect(window, self._drawcolor, (self._x1, self._y1, self._x_size, self._y_size), 25)

    def every_sec(self):
        super().every_sec()
        self.__delta_sec = self.__delta_sec - 1
        if self.__delta_sec < 0:
            self.__delta_sec = 14
            try:
                con = self.__get_connection()
                if con:
                    fritz_status = FritzStatus(con)
                    if fritz_status.is_connected:
                        self.__set_status_state(1)
                        if not self.__connection_up:
                            logging.info("connection to the internet up.")
                            self.__connection_up = True
                            self.__set_line_state(0)
                    else:
                        self.__set_status_state(0)
                        if self.__connection_up:
                            logging.warning("lost connection to the internet!")
                            self.__connection_up = False
            except (ConnectionError, requests.ReadTimeout):
                logging.warning("connection failed in every_sec()")
                self.__fritz_connection = None

        return False

    def every_hour(self):
        super().every_hour()
        if self.__init_telephonebook(False):
            self.__send_event('fb_incoming_call')
            self.__send_event('fb_outgoing_call')
            self.__send_event('fb_missed_call')

    def force_reload(self):
        self.__init_telephonebook(True)
        self.__init_calllists()
        self.__send_event('fb_incoming_call')
        self.__send_event('fb_outgoing_call')
        self.__send_event('fb_missed_call')
