#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import calendar
import datetime
import time

from .base_textbox_class import BaseTextboxClass


class DrawDate(BaseTextboxClass):
    # pylint: disable=too-many-arguments
    def __init__(self, config, event_types, x1_y1, x2_y2, inner_offset, frame, h_align, v_align, narrow):
        super().__init__(config, event_types, x1_y1, x2_y2, inner_offset, frame, h_align, v_align, 1,
                         narrow)

    def every_min(self):
        # get time
        if not self.is_access_exclusive():
            local_time = time.localtime()

            dateformat = self._config.get("date_format", "%a %e.%b %Y, KW %V", section=self.classname)
            datestr = time.strftime(dateformat, local_time)
            content = [datestr]
            self.set_content(content)

    def _exit_exclusive_access(self):
        super()._exit_exclusive_access()
        self.every_min()

    def exclusive_access(self, enter):
        super().exclusive_access(enter)
        if enter:
            self._set_new_size(0, 0, self._config.getint('size_x'), self._config.getint('size_y'))
            self._x_alignment = 0
            self._y_alignment = 0
            firstweekday = self._config.get("firstweekday", 0, section=self.classname)
            cal_entry = calendar.TextCalendar(firstweekday=firstweekday)
            newcontent = cal_entry.formatyear(datetime.datetime.now().year, c=3, m=4).split('\n')
            self._max_lines = len(newcontent)
            self.set_content(newcontent, True)

            box_width, box_height = self.calc_or_draw_text(False)
            box_width += self._inner_offset * 2
            box_height += self._inner_offset * 2
            weight = self._config.getint('size_x')
            height = self._config.getint('size_y')
            self._set_new_size((weight - box_width) / 2, (height - box_height) / 2, box_width, box_height)
