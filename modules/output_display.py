#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from distutils import spawn
import logging

import pygame

from .alertbox import AlertBox

TMP_IMAGE = "/tmp/images/temp.bmp"
BACKLIGHT_DEVICE = "/sys/class/backlight/fb_ili9341/bl_power"

TFT_INIT = "/usr/bin/tft_init"
TFT_CLEAR = "/usr/bin/tft_clear"
TFT_BMP = "/usr/bin/tft_bmp "
TFT_PWM = "/usr/bin/tft_pwm "


class OutputTFT:
    def __init__(self, config):
        self.__config = config
        self.__brightness = config.getint("__brightness", 10)
        self.__display_blocked = False
        self.__save_image_path = config.get("save_image_path")
        image_path = os.path.expanduser(config.get("background_image"))
        try:
            if os.path.isfile(image_path):
                image = pygame.image.load(image_path)
                x_size = config.getint('size_x')
                y_size = config.getint('size_y')
                if image.get_width() != x_size or image.get_height() != y_size:
                    image = pygame.transform.scale(image, (x_size, y_size))
                self.__config.setbackimage(image)
        except pygame.error as err:
            logging.error("Could not load Background Image! (%s)", err)

        color = config.getcolor("background_color")
        if color:
            config.setbackcolor(color)
        color = config.getcolor("foreground_color")
        if color:
            config.setdrawcolor(color)
        color = config.getcolor("frame_color")
        if color:
            config.setframecolor(color)

        self.__has_backlight_device = os.path.exists(BACKLIGHT_DEVICE)
        path = spawn.find_executable(TFT_INIT)
        if path:
            self.__passive_display = True
            os.putenv('SDL_VIDEODRIVER', 'dummy')
            os.system(TFT_INIT)
            os.system(TFT_CLEAR)
        else:
            self.__passive_display = False
            """
            Ininitializes a new pygame screen using the framebuffer
            """
            # Based on "Python GUI in Linux frame buffer"
            # http://www.karoltomala.com/blog/?p=679
            # taken from
            # https://learn.adafruit.com/pi-video-output-using-pygame/pointing-pygame-to-the-framebuffer
            disp_no = os.getenv("DISPLAY")
            if disp_no:
                logging.info(f"I'm running under X display = {disp_no}")
            else:
                # Check which frame buffer drivers are available
                # Start with fbcon since directfb hangs with composite output
                drivers = ['fbcon', 'directfb', 'svgalib']
                found = False
                for driver in drivers:
                    # Make sure that SDL_VIDEODRIVER is set
                    if not os.getenv('SDL_VIDEODRIVER'):
                        os.putenv('SDL_VIDEODRIVER', driver)
                    try:
                        pygame.display.init()
                    except pygame.error:
                        logging.error(f'Driver: {driver} failed.')
                        continue
                    found = True
                    break

                if not found:
                    raise Exception('No suitable video driver found!')
        self.__backlight_state = True
        self.backlight_on()

    def draw(self, drawlist=None):
        window = self.__config.getwindow()
        # window clear
        window.fill(self.__config.getbackcolor())

        if self.__config.getbackimage():
            window.blit(self.__config.getbackimage(), (0, 0))

        if drawlist:
            drawpoints = iter(drawlist)
            try:
                endpoint = next(drawpoints)
                while True:
                    startpoint = endpoint
                    endpoint = next(drawpoints)
                    pygame.draw.line(window, self.__config.getdrawcolor(), startpoint, endpoint)
            except StopIteration:
                pass

    def alert(self, message):
        window = self.__config.getwindow()
        lines = message.split('\n')

        top = window.get_height() / 4
        left = window.get_width() / 5

        alert = AlertBox(self.__config, (left, top), (left * 3, top * 2))
        alert.draw_alert(lines)

        self.update()
        self.__display_blocked = True

    def alert_off(self):
        self.__display_blocked = False
        self.update()

    def is_alert_active(self):
        return self.__display_blocked

    def update(self):
        if not self.__display_blocked:
            window = self.__config.getwindow()
            if self.__save_image_path:
                try:
                    pygame.image.save(window, self.__save_image_path)
                except pygame.error:
                    logging.error("'%s' could not be saved; Saving of Image will be disabled!", self.__save_image_path)
                    self.__save_image_path = ""
            if self.__passive_display:
                pygame.image.save(window, TMP_IMAGE)
                os.system(TFT_BMP + TMP_IMAGE)
            else:
                if self.__config.getboolean('flip_screen', False):
                    window.blit(pygame.transform.flip(window, True, True), (-1, 1))
                pygame.display.update()

    def backlight_on(self):
        self.__backlight_state = True
        if self.__passive_display:
            os.system(TFT_PWM + str(self.__brightness))
        elif self.__has_backlight_device:
            with open(BACKLIGHT_DEVICE, "w", encoding="utf8") as backlight:
                backlight.write("0")
                backlight.close()

    def backlight_off(self):
        self.__backlight_state = False
        if self.__passive_display:
            os.system(TFT_PWM + "0")
        elif self.__has_backlight_device:
            with open(BACKLIGHT_DEVICE, "w", encoding="utf8") as backlight:
                backlight.write("1")
                backlight.close()

    def backlight_toggle(self):
        self.__backlight_state = not self.__backlight_state
        if self.__passive_display:
            os.system(TFT_PWM + "0")
        elif self.__has_backlight_device:
            val = self.__backlight__()
            val = 1 - val
            with open(BACKLIGHT_DEVICE, "w", encoding="utf8") as backlight:
                backlight.write(str(val))
                backlight.close()

    def __backlight__(self):
        if self.__passive_display:
            pass
        elif self.__has_backlight_device:
            with open(BACKLIGHT_DEVICE, encoding="utf8") as backlight:
                val = backlight.read()
                backlight.close()
            self.__backlight_state = val == 1
            return int(val)
        return 0
