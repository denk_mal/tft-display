#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pygame

from .base_textbox_class import BaseTextboxClass


class AlertBox(BaseTextboxClass):
    def __init__(self, config, x1_y1, x2_y2):
        super().__init__(config, [], x1_y1, x2_y2, 10, 15, 1, 1, 5, True)

    def _drawframe(self):
        window = self._config.getwindow()
        if self._config.getboolean('flip_screen', False):
            window.blit(pygame.transform.flip(window, True, True), (-1, 1))
        if self._config.getbackimage():
            window.blit(self._config.getbackimage(), (self._x1, self._y1),
                        (self._x1, self._y1, self._x_size, self._y_size))
        else:
            pygame.draw.rect(window, self._backgroundcolor, (self._x1, self._y1, self._x_size, self._y_size))
        pygame.draw.rect(window, self._drawcolor, (self._x1, self._y1, self._x_size, self._y_size), 10)

    def draw_alert(self, content):
        self.set_content(content)

        super().draw()
