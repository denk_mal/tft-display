#!/usr/bin/env python3
# -*- coding: utf-8 -*-

WHITE_COLOR = (0xff, 0xff, 0xff)
BLACK_COLOR = (0x00, 0x00, 0x00)
RED_COLOR = (0xff, 0x00, 0x00)
GREEN_COLOR = (0x00, 0xff, 0x00)
