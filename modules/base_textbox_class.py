#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pygame

from .base_draw_class import BaseDrawClass


class BaseTextboxClass(BaseDrawClass):
    # pylint: disable=too-many-instance-attributes,too-many-arguments
    def __init__(self, config, event_types, x1_y1, x2_y2, inner_offset, frame, h_align, v_align, maxlines, narrow):
        self._fontname = config.get('fontname')
        self._fontsize = 0
        self._x_alignment = h_align  # 0 = left, 1 = center, 2 = right
        self._y_alignment = v_align  # 0 = top, 1 = center, 2 = bottom
        # draw strings
        self._max_lines = maxlines
        self._headtext = ""
        self._contenttext = []
        self._narrow_lines = narrow
        super().__init__(config, event_types, x1_y1, x2_y2, inner_offset, frame)

    def _set_new_size(self, x_1, y_1, x_2, y_2):
        super()._set_new_size(x_1, y_1, x_2, y_2)
        self.__calculate_content_fontsize()

    # pylint: disable=attribute-defined-outside-init
    def _enter_exclusive_access(self):
        if self.is_access_exclusive():
            return
        super()._enter_exclusive_access()
        self._fontname_excl = self._fontname
        self._fontsize_excl = self._fontsize
        self._x_alignment_excl = self._x_alignment
        self._y_alignment_excl = self._y_alignment
        self._max_lines_excl = self._max_lines
        self._headtext_excl = self._headtext
        self._contenttext_excl = self._contenttext
        self._narrow_lines_excl = self._narrow_lines

    # pylint: disable=attribute-defined-outside-init
    def _exit_exclusive_access(self):
        if not self.is_access_exclusive():
            return
        super()._exit_exclusive_access()
        self._fontname = self._fontname_excl
        self._fontsize = self._fontsize_excl
        self._x_alignment = self._x_alignment_excl
        self._y_alignment = self._y_alignment_excl
        self._max_lines = self._max_lines_excl
        self._headtext = self._headtext_excl
        self._contenttext = self._contenttext_excl
        self._narrow_lines = self._narrow_lines_excl

    def __calculate_content_fontsize(self):
        # calculate best fontsize for text
        tstline = ""
        for labeltxt in self._contenttext:
            if len(labeltxt) > len(tstline):
                tstline = labeltxt
        if tstline:
            self.calculate_fontsize(tstline, height=self._y_size / self._max_lines)

    def calculate_fontsize(self, string, width=0, height=0):
        self._fontsize = 0
        if height == 0:
            height = self._y_off_size
        if width == 0:
            width = self._x_off_size
        while True:
            font_normal = pygame.font.SysFont(self._fontname, self._fontsize)
            if self._narrow_lines:
                height_cal = font_normal.get_height()
            else:
                height_cal = font_normal.get_ascent()
            # height_cal = font_normal.get_height()
            width_cal = font_normal.size(string)[0]
            if width_cal > width:
                self._fontsize -= 1
                break
            if height_cal > height:
                # self._fontsize -= 1
                break
            self._fontsize += 1

    def set_horizontal_alignment(self, alignment):
        self._x_alignment = alignment

    def set_vertical_alignment(self, alignment):
        self._y_alignment = alignment

    def set_header(self, headertext):
        self._headtext = headertext

    def set_content(self, content, preformatted=False):
        self._contenttext = []
        max_lines = self._max_lines
        if self._headtext:
            if isinstance(self._headtext, list):
                max_lines += len(self._headtext)
                self._contenttext = self._headtext
            if isinstance(self._headtext, str):
                max_lines += 1
                self._contenttext = [self._headtext]
        if preformatted:
            self._contenttext.extend(list(content)[:max_lines])
        else:
            self._contenttext.extend(list(map(str.strip, content))[:max_lines])
        self.__calculate_content_fontsize()

    def set_narrowed_lines(self, value=True):
        self._narrow_lines = value

    def draw(self):
        super().draw()
        self.calc_or_draw_text()

    def calc_or_draw_text(self, draw=True):
        font_normal = pygame.font.SysFont(self._fontname, self._fontsize)

        # draw text from content list
        # y_offset = font_normal.get_ascent()
        if self._narrow_lines:
            y_offset = font_normal.get_ascent()
        else:
            y_offset = font_normal.get_height()
        y_base = self._y1_off
        delta_y = self._y_off_size - y_offset * len(self._contenttext)
        if self._y_alignment == 1:
            y_base += delta_y / 2
        elif self._y_alignment == 2:
            y_base += delta_y

        width = 0
        for idx, labeltxt in enumerate(self._contenttext):
            if idx >= self._max_lines:
                break
            label = font_normal.render(labeltxt, True, self._drawcolor)
            x_offset = font_normal.size(labeltxt)[0]
            width = max(x_offset, width)
            x_offset = (self._x_off_size - x_offset) / 2
            x_offset = self._x1_off + (x_offset * self._x_alignment)
            x_offset = max(x_offset, 0)
            if draw:
                self._config.getwindow().blit(label, (x_offset, y_base + (y_offset * idx)))
        return width, y_offset * len(self._contenttext)
