#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import datetime

import pygame

from .tph_wrapper import TPHWrapper
from .base_textbox_class import BaseTextboxClass

SAVED_VALUES_COUNT = 180  # must be a multiple of 60! (minutes per hour)
MAX_SCALE_FACTOR = 500


class DrawHumidity(BaseTextboxClass):
    # pylint: disable=too-many-arguments
    def __init__(self, config, event_types, x1_y1, x2_y2, inner_offset, frame, h_align, v_align, narrow):
        super().__init__(config, event_types, x1_y1, x2_y2, inner_offset, frame, h_align, v_align, 1,
                         narrow)
        self.__tph_wrapper = TPHWrapper(config)

        humidity = self.__tph_wrapper.get_humidity()
        if humidity != self.__tph_wrapper.error_value:
            humiditystr = f"{humidity:.1f} %"
            content = [humiditystr]
            self.set_content(content)

        self.rrd_humidity = []

        self.__width = self._config.getint('size_x')
        self.__height = self._config.getint('size_y')
        self.__scale_x, self.__left = divmod(self.__width, SAVED_VALUES_COUNT)

    def post_init(self):
        humidity = self.__tph_wrapper.get_humidity()
        for _unused in range(SAVED_VALUES_COUNT):
            self.rrd_humidity.append(humidity)

    # pylint: disable=too-many-locals
    def draw(self):
        if self.is_access_exclusive():
            window = self._config.getwindow()
            pygame.draw.line(window, self._drawcolor, (self.__left, 0), (self.__left, self.__height))
            pygame.draw.line(window, self._drawcolor, (self.__left, self.__height / 2),
                             (self.__width, self.__height / 2))
            min_humidity = min(self.rrd_humidity)
            max_humidity = max(self.rrd_humidity)
            diff_humidity = max_humidity - min_humidity
            left_start = self.__left
            if diff_humidity > (self.__height / MAX_SCALE_FACTOR):
                scale_y, top = divmod(self.__height, diff_humidity)
            else:
                scale_y = MAX_SCALE_FACTOR
                top = self.__height - diff_humidity * MAX_SCALE_FACTOR
            offset = self.__height - (top / 2)
            now = datetime.datetime.now()
            minute = now.minute + int(now.hour % (SAVED_VALUES_COUNT / 60)) * 60 + 1
            if minute >= SAVED_VALUES_COUNT:
                minute = 0
            line_start = offset - ((self.rrd_humidity[minute] - min_humidity) * scale_y)
            for index in range(1, SAVED_VALUES_COUNT):
                real_index = minute + index
                if real_index >= SAVED_VALUES_COUNT:
                    real_index -= SAVED_VALUES_COUNT
                left_end = left_start + self.__scale_x
                line_end = offset - ((self.rrd_humidity[real_index] - min_humidity) * scale_y)
                pygame.draw.line(window, self._drawcolor, (left_start, line_start), (left_end, line_end))
                left_start = left_end
                line_start = line_end
        else:
            super().draw()

    def every_min(self):
        humidity = self.__tph_wrapper.get_humidity()
        if humidity != self.__tph_wrapper.error_value:
            now = datetime.datetime.now()
            minute = now.minute + int(now.hour % (SAVED_VALUES_COUNT / 60)) * 60
            self.rrd_humidity[minute] = humidity

            humiditystr = f"{humidity:.1f} %"
        else:
            humiditystr = "--.- %"

        content = [humiditystr]
        self.set_content(content)
