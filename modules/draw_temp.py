#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pygame

from .tph_wrapper import TPHWrapper
from .base_textbox_class import BaseTextboxClass


class DrawTemp(BaseTextboxClass):
    # pylint: disable=too-many-arguments
    def __init__(self, config, event_types, x1_y1, x2_y2, inner_offset, frame, h_align, v_align, narrow):
        super().__init__(config, event_types, x1_y1, x2_y2, inner_offset, frame, h_align, v_align, 1,
                         narrow)
        self.__old_temp = 850
        self.__temp = self.__old_temp
        self.__tph_wrapper = TPHWrapper(config)

        # this should calculate the approximate font site for temp values
        # between -9.9 and 99.9 degree Celsius
        self.calculate_fontsize("888")

    def __draw_local_temp(self):
        font_big = pygame.font.SysFont(self._fontname, self._fontsize)
        font_normal = pygame.font.SysFont(self._fontname, int(self._fontsize / 2))

        temp_int1 = int(self.__temp / 10)
        temp_int2 = int((self.__temp - temp_int1 * 10))
        tmpstr1 = f'{temp_int1:2d}°'
        tmpstr1b = f'{temp_int1:2d}'
        tmpstr2 = f',{temp_int2:1d}'
        label1 = font_big.render(tmpstr1, True, self._drawcolor)
        label2 = font_normal.render(tmpstr2, True, self._drawcolor)

        x1_offset = ((self._x_size - font_big.size(tmpstr1)[0]) / 2) + self._x1
        x2_offset = x1_offset + font_big.size(tmpstr1b)[0]
        if x2_offset < self._x1_off:
            x2_offset = self._x1_off

        asc1 = font_big.get_ascent()
        asc2 = font_normal.get_ascent()

        self._config.getwindow().blit(label1, (x1_offset, self._y1))
        self._config.getwindow().blit(label2, (x2_offset, self._y1 + (asc1 - asc2)))

    def draw(self):
        super().draw()
        self.__draw_local_temp()

    def every_min(self):
        temp = self.__tph_wrapper.get_temperature()
        if temp == self.__tph_wrapper.error_value:
            self.__temp = self.__old_temp
        else:
            self.__temp = int(float(temp) * 10)
            self.__old_temp = self.__temp

    # the box name is empty for the main box and contains the dynamic box name for dynamic boxes
    def click_event(self, click_pos, box_name):
        pass
