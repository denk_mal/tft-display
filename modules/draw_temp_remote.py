#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import datetime
import locale
import pathlib

import pygame

from .base_textbox_class import BaseTextboxClass
from .sun import Sun


class DrawTempRemote(BaseTextboxClass):
    # pylint: disable=too-many-arguments, too-many-instance-attributes
    def __init__(self, config, event_types, x1_y1, x2_y2, inner_offset, frame, h_align, v_align, narrow):
        super().__init__(config, event_types, x1_y1, x2_y2, inner_offset, frame, h_align, v_align, 2, narrow)
        self.__info1 = "<sunset/sunrise>"
        self.__info2 = "<no data>"
        self.__info_temp_hot = config.getfloat("hot_temp_threshold", 21.0, self.classname)
        self.__info_temp_cold = config.getfloat("cold_temp_threshold", 3.0, self.classname)
        self.__info_temp = self.__info_temp_hot
        self.__info_color_hot = config.getcolor("alert_bg_color_hot", (255, 0, 0), self.classname)
        self.__info_color_cold = config.getcolor("alert_bg_color_cold", (0, 0, 255), self.classname)
        self.__latitude = config.get("latitude", "53.54848607251756", self.classname)
        self.__longitude = config.get("longitude", "9.978815989009288", self.classname)
        self.__temperature_file = config.get("temperature_file", "", self.classname)

        self.calculate_fontsize(self.__info1, height=self._y_size / 2)

    def post_init(self):
        self.every_hour()

    def __converttime(self, key, time_val):
        line = f"{key}: {time_val}"
        try:
            fmtstr = key + ": %H:%M"
            fmtstr = self._config.get(key, fmtstr, self.classname)
            line = time_val.strftime(fmtstr)
        except ValueError:
            pass
        return line

    def __get_remote_temp(self):
        self.__info1 = "<getting data>"
        self.__info2 = "<None>"
        file = pathlib.Path(self.__temperature_file)
        if not file.is_file():
            return
        try:
            lines = file.read_text(encoding=locale.getencoding()).split(';')
            temperature_remote = lines[-1].strip()
            if len(lines) > 2:
                location = lines[-2].strip()
            else:
                location = ""
            self.__info1 = location
            self.__info2 = f"{temperature_remote}°C"
            self.__info_temp = float(temperature_remote)
        except (FileNotFoundError, ValueError):
            pass

    def __blit_line(self, info_txt, font_base, y_base):
        label = font_base.render(info_txt, True, self._drawcolor)
        x_offset = font_base.size(info_txt)[0]
        x_offset = (self._x_off_size - x_offset) / 2
        x_offset = self._x1_off + (x_offset * self._x_alignment)
        x_offset = max(x_offset, 0)
        self._config.getwindow().blit(label, (x_offset, y_base))

    def __draw_temp(self):
        font_big = pygame.font.SysFont(self._fontname, int(self._fontsize * 2.5))
        font_normal = pygame.font.SysFont(self._fontname, self._fontsize)

        y_offset = font_normal.get_ascent()
        y_base = self._y1_off

        self.__blit_line(self.__info1, font_normal, y_base)

        y_base += (y_offset * 1)

        self.__blit_line(self.__info2, font_big, y_base)

    def draw(self):
        if self.__info_temp > self.__info_temp_hot:
            self.set_backgroundcolor(self.__info_color_hot, True)
        elif self.__info_temp < self.__info_temp_cold:
            self.set_backgroundcolor(self.__info_color_cold, True)
        else:
            self.set_backgroundcolor(None)

        super().draw()
        self.__draw_temp()

    def every_min(self):
        self.__get_remote_temp()

    def every_sec(self):
        return False

    def every_hour(self):
        mytz = datetime.datetime.now(tz=datetime.timezone(datetime.timedelta(hours=2)))
        sun_info = Sun(mytz, float(self.__latitude), float(self.__longitude))
        infoline = [self.__converttime('sunrise', sun_info.sunrise()), self.__converttime('sunset', sun_info.sunset())]
        pygame.event.post(pygame.event.Event(self._event_types[1], target_type='textbox', id='temp_info',
                                             data=infoline, receiver=self))

    # the box name is empty for the main box and contains the dynamic box name for dynamic boxes
    def click_event(self, click_pos, box_name):
        pass
