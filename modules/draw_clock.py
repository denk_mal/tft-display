#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import time

import pygame

from .base_draw_class import BaseDrawClass
from .common_data import RED_COLOR


def deg2rad(deg):
    return math.pi / 180 * deg


def calc_coord(deg):
    return math.cos(deg2rad(deg - 90)), math.sin(deg2rad(deg - 90))


class DrawClock(BaseDrawClass):
    # pylint: disable=too-many-arguments
    def __init__(self, config, event_types, x1_y1, x2_y2, inner_offset, frame, _h_align, _v_align, _narrow):
        super().__init__(config, event_types, x1_y1, x2_y2, inner_offset, frame)

    # pylint: disable=attribute-defined-outside-init
    def _set_new_size(self, x_1, y_1, x_2, y_2):
        super()._set_new_size(x_1, y_1, x_2, y_2)
        self.__x1_center = x_2 / 2 + x_1
        self.__y1_center = y_2 / 2 + y_1
        self.__radius = min(self.__x1_center, self.__y1_center) - self._inner_offset
        self._long_hand = []
        self._short_hand = []
        self._inner_plate = []
        for degree in range(0, 60):
            self._long_hand.append(self.__calc_line(degree))
            self._short_hand.append(self.__calc_line(degree, 2/3))
            self._inner_plate.append(self.__calc_line(degree, 3/4))

    def __calc_line(self, degree, factor=1.0):
        coord = calc_coord(degree * 6)
        x_pos = coord[0] * self.__radius * factor + self.__x1_center
        y_pos = coord[1] * self.__radius * factor + self.__y1_center
        return x_pos, y_pos

    def __draw_plate(self):
        for degree in range(0, 60, 5):
            x1_pos, y1_pos = self._inner_plate[degree]
            x2_pos, y2_pos = self._long_hand[degree]
            pygame.draw.line(self._config.getwindow(), self._drawcolor, (x1_pos, y1_pos), (x2_pos, y2_pos), 5)

    def draw(self):
        # get time
        local_time = time.localtime()

        super().draw()
        # draw circle
        self.__draw_plate()
        # draw hour
        x_pos, y_pos = self._short_hand[(local_time.tm_hour % 12) * 5 + local_time.tm_min // 12]
        pygame.draw.line(self._config.getwindow(), self._drawcolor,
                         (self.__x1_center, self.__y1_center), (x_pos, y_pos), 3)
        # draw min
        x_pos, y_pos = self._long_hand[local_time.tm_min]
        pygame.draw.line(self._config.getwindow(), self._drawcolor,
                         (self.__x1_center, self.__y1_center), (x_pos, y_pos), 3)
        # draw sec
        x_pos, y_pos = self._long_hand[local_time.tm_sec]
        pygame.draw.line(self._config.getwindow(), RED_COLOR,
                         (self.__x1_center, self.__y1_center), (x_pos, y_pos))

    # pylint: disable=no-self-use
    def every_sec(self):
        return True

    def _exit_exclusive_access(self):
        super()._exit_exclusive_access()
        self._set_new_size(self._x1, self._y1, self._x_size, self._y_size)

    def exclusive_access(self, enter):
        super().exclusive_access(enter)
        if enter:
            weight = self._config.getint('size_x')
            height = self._config.getint('size_y')
            min_x_y = min(weight, height)
            if weight > height:
                x_pos = (weight - height) / 2
                y_pos = 0
            else:
                x_pos = 0
                y_pos = (height - weight) / 2
            self._set_new_size(x_pos, y_pos, min_x_y, min_x_y)
