#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is a wrapper for various sensors
# providing data for temperature, pressure and humidity

import os

import rrdtool


class TPHWrapper:
    # pylint: disable=import-outside-toplevel
    def __init__(self, config):
        self.error_value = config.getfloat("err_value", -85.0, "tph_wrapper")
        self.__rrd_database_file = config.get("rrd_database_file")

    def __parse_json(self, key):
        ret_val = self.error_value
        file = self.__rrd_database_file
        if not os.path.isfile(file):
            return ret_val
        response = rrdtool.lastupdate(file)
        if 'ds' in response.keys():
            data = response['ds']
            if key in data.keys():
                ret_val = data[key]

        return ret_val

    def get_temperature(self) -> float:
        return self.__parse_json("temperature")

    def get_pressure(self) -> float:
        return self.__parse_json("pressure")

    def get_humidity(self) -> float:
        return self.__parse_json("humidity")

    def get_temperature_remote(self) -> float:
        return self.__parse_json("temperature_remote")


if __name__ == "__main__":
    import sys

    SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
    sys.path.append(os.path.dirname(SCRIPT_DIR))
    from modules.config import Config

    config_in = Config()
    one = TPHWrapper(config_in)
    two = TPHWrapper(config_in)
    temperature_out = one.get_temperature()
    pressure_out = one.get_pressure()
    humidity_out = one.get_humidity()
    temperature_remote = one.get_temperature_remote()
    print(f"Temperature : {temperature_out:.1f}")
    print(f"Pressure    : {pressure_out:.1f}")
    print(f"Humidity    : {humidity_out:.1f}")
    print(f"Temperature : {temperature_remote:.1f}")
