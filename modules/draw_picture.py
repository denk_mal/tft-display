#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
import os
import random

import pygame

from .base_draw_class import BaseDrawClass


class DrawPicture(BaseDrawClass):
    # pylint: disable=too-many-arguments,too-many-instance-attributes
    def __init__(self, config, event_types, x1_y1, x2_y2, inner_offset, frame, _h_align, _v_align, _narrow):
        super().__init__(config, event_types, x1_y1, x2_y2, inner_offset, frame)
        self.__picturefolder = os.path.expanduser(config.get("imagefolder", '', self.classname))
        self.__time_interval = config.getint("time_interval", 5, self.classname)
        self.__files = []
        self.__image = None
        self.__picture = None
        self.__picture_x_offset = 0
        self.__picture_y_offset = 0
        self.__idx = 0
        self.__random = config.getboolean("random", False, self.classname)
        self.__cur_interval = 0
        self.__refresh_imagelist()

    def __refresh_imagelist(self):
        if os.path.isdir(self.__picturefolder):
            self.__files = [fn for fn in os.listdir(self.__picturefolder)
                            if any(fn.lower().endswith(ext) for ext in ['.jpg', '.jpeg', '.bmp', '.png', '.gif'])]
        self.__files.sort(key=str.lower)
        self.__idx = len(self.__files)
        self.__frame_counter = len(self.__files)

    # aspect_scale.py - Scaling surfaces keeping their aspect ratio
    # Raiser, Frank - Sep 6, 2k++
    # crashchaos at gmx.net
    # taken from: http://www.pygame.org/pcr/transform_scale/
    def __scale_image(self):
        if not self.__image:
            return
        # Scales 'img' to fit into box bx/by.
        # This method will retain the original image's aspect ratio
        box_x = self._x_off_size
        box_y = self._y_off_size
        image_x, image_y = self.__image.get_size()
        if image_x > image_y:
            # fit to width
            scale_factor = box_x / float(image_x)
            scaly_y = scale_factor * image_y
            if scaly_y > box_y:
                scale_factor = box_y / float(image_y)
                scale_x = scale_factor * image_x
                scaly_y = box_y
            else:
                scale_x = box_x
        else:
            # fit to height
            scale_factor = box_y / float(image_y)
            scale_x = scale_factor * image_x
            if scale_x > box_x:
                scale_factor = box_x / float(image_x)
                scale_x = box_x
                scaly_y = scale_factor * image_y
            else:
                scaly_y = box_y
        scale_x = int(scale_x)
        scaly_y = int(scaly_y)
        if box_x - scale_x:
            self.__picture_x_offset = ((box_x - scale_x) // 2) + self._inner_offset
            self.__picture_y_offset = self._inner_offset
        else:
            self.__picture_x_offset = self._inner_offset
            self.__picture_y_offset = ((box_y - scaly_y) // 2) + self._inner_offset
        self.__picture = pygame.transform.scale(self.__image, (scale_x, scaly_y))

    def __draw(self, x_pos, y_pos):
        try:
            window = self._config.getwindow()
            window.blit(self.__picture, (x_pos, y_pos))
        except pygame.error:
            pass

    def draw(self):
        super().draw()
        if self.__picture:
            self.__draw(self._x1 + self.__picture_x_offset, self._y1 + self.__picture_y_offset)

    def every_min(self):
        try:
            if self.__files and not self.__cur_interval:
                if self.__random:
                    self.__idx = random.randint(0, len(self.__files) - 1)
                else:
                    if self.__idx >= len(self.__files):
                        self.__idx = 0
                    else:
                        self.__idx += 1
                self.__image = pygame.image.load(os.path.join(self.__picturefolder, self.__files[self.__idx]))
                self.__scale_image()
                self.__cur_interval = self.__time_interval
                self.__frame_counter -= 1
                if self.__frame_counter <= 0:
                    self.__refresh_imagelist()
            self.__cur_interval -= 1
        except pygame.error:
            logging.error("loading of pictures failed!")

    def exclusive_access(self, enter):
        super().exclusive_access(enter)
        if enter:
            self._set_new_size(0, 0, self._config.getint('size_x'), self._config.getint('size_y'))
        self.__scale_image()
