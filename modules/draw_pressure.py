#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import datetime

import pygame

from .tph_wrapper import TPHWrapper
from .base_textbox_class import BaseTextboxClass

SAVED_VALUES_COUNT = 180  # must be a multiple of 60! (minutes per hour)
MAX_SCALE_FACTOR = 500


class DrawPressure(BaseTextboxClass):
    # pylint: disable=too-many-arguments
    def __init__(self, config, event_types, x1_y1, x2_y2, inner_offset, frame, h_align, v_align, narrow):
        super().__init__(config, event_types, x1_y1, x2_y2, inner_offset, frame, h_align, v_align, 1,
                         narrow)
        self.__tph_wrapper = TPHWrapper(config)
        self.rrd_pressure = []
        self.__state = 255  # set to anything invalid to make sure that first call of __set_state() would work

        self.__width = self._config.getint('size_x')
        self.__height = self._config.getint('size_y')
        self.__scale_x, self.__left = divmod(self.__width, SAVED_VALUES_COUNT)

    def post_init(self):
        pressure = self.__tph_wrapper.get_pressure()
        for _unused in range(SAVED_VALUES_COUNT):
            self.rrd_pressure.append(pressure)
        self.set_state(0)

    # pylint: disable=too-many-locals
    def draw(self):
        if self.is_access_exclusive():
            window = self._config.getwindow()
            pygame.draw.line(window, self._drawcolor, (self.__left, 0), (self.__left, self.__height))
            pygame.draw.line(window, self._drawcolor, (self.__left, self.__height / 2),
                             (self.__width, self.__height / 2))
            min_pressure = min(self.rrd_pressure)
            max_pressure = max(self.rrd_pressure)
            diff_pressure = max_pressure - min_pressure
            left_start = self.__left
            if diff_pressure > (self.__height / MAX_SCALE_FACTOR):
                scale_y, top = divmod(self.__height, diff_pressure)
            else:
                scale_y = MAX_SCALE_FACTOR
                top = self.__height - diff_pressure * MAX_SCALE_FACTOR
            offset = self.__height - (top / 2)
            now = datetime.datetime.now()
            minute = now.minute + int(now.hour % (SAVED_VALUES_COUNT / 60)) * 60 + 1
            if minute >= SAVED_VALUES_COUNT:
                minute = 0
            line_start = offset - ((self.rrd_pressure[minute] - min_pressure) * scale_y)
            for index in range(1, SAVED_VALUES_COUNT):
                real_index = minute + index
                if real_index >= SAVED_VALUES_COUNT:
                    real_index -= SAVED_VALUES_COUNT
                left_end = left_start + self.__scale_x
                line_end = offset - ((self.rrd_pressure[real_index] - min_pressure) * scale_y)
                pygame.draw.line(window, self._drawcolor, (left_start, line_start), (left_end, line_end))
                left_start = left_end
                line_start = line_end
        else:
            super().draw()

    def set_state(self, new_state):
        if new_state == self.__state:
            return
        self.__state = new_state
        type_even = '_REMOVE_'
        type_up = '_REMOVE_'
        type_down = '_REMOVE_'
        if self.__state == 0:
            type_even = 'drawbox'
        elif self.__state == 1:
            type_up = 'drawbox'
        elif self.__state == -1:
            type_down = 'drawbox'
        pygame.event.post(pygame.event.Event(self._event_types[1], target_type=type_even,
                                             id='pressure_even', receiver=self))
        pygame.event.post(pygame.event.Event(self._event_types[1], target_type=type_up,
                                             id='pressure_up', receiver=self))
        pygame.event.post(pygame.event.Event(self._event_types[1], target_type=type_down,
                                             id='pressure_down', receiver=self))

    def every_min(self):
        pressure = self.__tph_wrapper.get_pressure()
        if pressure != self.__tph_wrapper.error_value:
            now = datetime.datetime.now()
            minute = now.minute + int(now.hour % (SAVED_VALUES_COUNT / 60)) * 60
            self.rrd_pressure[minute] = pressure
            min_pressure = min(self.rrd_pressure)
            max_pressure = max(self.rrd_pressure)
            oldpressure = self.rrd_pressure[minute]

            # check if an obseravble difference exists
            if (max_pressure - min_pressure) > 1 and ((max_pressure - pressure) > 1 or (pressure - min_pressure) > 1):
                min_minute = -1
                max_minute = -1
                # check if one of the extremes exist before the actual pressure value
                for index in range(minute):
                    if self.rrd_pressure[index] == min_pressure:
                        min_minute = index
                    if self.rrd_pressure[index] == max_pressure:
                        max_minute = index
                # if none of the extremes found search for extremes older data
                if min_minute == -1 and max_minute == -1:
                    for index in range(minute, len(self.rrd_pressure)):
                        if self.rrd_pressure[index] == min_pressure:
                            min_minute = index
                        if self.rrd_pressure[index] == max_pressure:
                            max_minute = index
                # check if the newest extreme is minimum pressure value
                if min_minute > max_minute:
                    oldpressure = max_pressure
                    # supress jitter
                    if (pressure - min_pressure) > 0.2:
                        oldpressure = min_pressure
                else:
                    oldpressure = min_pressure
                    # supress jitter
                    if (max_pressure - pressure) > 0.2:
                        oldpressure = max_pressure

            if pressure - oldpressure > 1:
                self.set_state(1)
            elif oldpressure - pressure > 1:
                self.set_state(-1)
            else:
                self.set_state(0)
            infoline = [f"min: {min_pressure:.1f} hPa",
                        f"max: {max_pressure:.1f} hPa"]
            pygame.event.post(
                pygame.event.Event(self._event_types[1], target_type='textbox', id='pressure_info',
                                   data=infoline, receiver=self))
            pressurestr = f"{pressure:.1f}"
        else:
            pressurestr = "----.-"
            self.set_state(0)
            pygame.event.post(
                pygame.event.Event(self._event_types[1], target_type='textbox', id='pressure_info',
                                   data=["<pressure>", "<no data>"], receiver=self))

        content = [pressurestr]
        self.set_content(content)
