#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pygame

from .base_draw_class import BaseDrawClass


class DynDraw(BaseDrawClass):
    # pylint: disable=too-many-arguments
    def __init__(self, config, event_types, x1_y1, x2_y2, inner_offset, frame, drawtype, color):
        super().__init__(config, event_types, x1_y1, x2_y2, inner_offset, frame)
        self.drawtype = drawtype
        self.color = color

    def set_color(self, color):
        self.color = color

    def draw(self):
        super().draw()
        window = self._config.getwindow()
        if self.drawtype == 1:  # circle
            radius = int(min(self._x_off_size, self._y_off_size) / 2)
            pos = (int(self._x1 + self._x_size / 2), int(self._y1 + self._y_size / 2))
            pygame.draw.circle(window, self.color, pos, radius)
        elif self.drawtype == 2:  # triangle, peak on top
            polygon = [(self._x1_off + int(self._x_off_size / 2), self._y1_off),
                       (self._x1_off, self._y2_off),
                       (self._x2_off, self._y2_off),
                       (self._x1_off + int(self._x_off_size / 2), self._y1_off)]
            pygame.draw.polygon(window, self.color, polygon)
        elif self.drawtype == 3:  # triangle, peak on bottom
            polygon = [(self._x1_off + int(self._x_off_size / 2), self._y2_off),
                       (self._x1_off, self._y1_off),
                       (self._x2_off, self._y1_off),
                       (self._x1_off + int(self._x_off_size / 2), self._y2_off)]
            pygame.draw.polygon(window, self.color, polygon)
        elif self.drawtype == 4:  # square
            rectangle = (self._x1_off, self._y1_off, self._x_off_size, self._y_off_size)
            pygame.draw.rect(window, self.color, rectangle)
