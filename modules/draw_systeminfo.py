#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import datetime
import logging
import platform
import socket
import subprocess

from .base_textbox_class import BaseTextboxClass


def execute_cmd(command):
    result = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True)
    return result.decode()


class DrawSysteminfo(BaseTextboxClass):
    # pylint: disable=too-many-arguments
    def __init__(
        self,
        config,
        event_types,
        x1_y1,
        x2_y2,
        inner_offset,
        frame,
        h_align,
        v_align,
        narrow,
        interface="eth0",
    ):
        super().__init__(
            config,
            event_types,
            x1_y1,
            x2_y2,
            inner_offset,
            frame,
            h_align,
            v_align,
            3,
            narrow,
        )
        self.start_time = datetime.datetime.now()
        self.__fqdn = socket.getfqdn()
        self.__interface = interface
        self.__ip_addr = "127.0.0.1"
        self.__ip_ok = False
        self.__get_ip_info()

    @staticmethod
    def __get_interfacename(ifname):
        ifname_new = ""
        result = execute_cmd(
            "/bin/ip -o link show|awk '{print $2,$9}'|grep 'UP'|awk '{print $1}'"
        )
        ifaces = result.split("\n")
        for iface in ifaces:
            if iface == ifname:
                return ifname
            if not ifname_new:
                ifname_new = iface
        ifname_new = ifname_new[: ifname_new.find(":")]
        return ifname_new

    @staticmethod
    def __get_ip_address(ifname):
        # print(ifname)
        result = execute_cmd("/bin/ip -f inet addr show " + ifname).split("\n")
        line = ""
        for line in result:
            if line.find("inet ") != -1:
                break
        result = line.split(" ")
        for idx, elem in enumerate(result):
            if elem == "inet":
                return result[idx + 1]
        return ""

    def __get_ip_info(self):
        interface = self.__interface
        try:
            logging.debug("try to get interface")
            interface = self.__get_interfacename(self.__interface)
            logging.debug("found interface '%s'", interface)
            self.__ip_addr = self.__get_ip_address(interface)
            logging.debug("found address '%s'", self.__ip_addr)
            self.__fqdn = socket.getfqdn()
            self.__ip_ok = True
        except IndexError:
            self.__ip_addr = f"{interface} not found!"

    def _get_uptime(self):
        with open("/proc/uptime", encoding="utf8") as proc_uptime:
            uptime_seconds = int(float(proc_uptime.readline().split()[0]) / 60) * 60
            uptime_string = str(datetime.timedelta(seconds=uptime_seconds))
            uptime_string = uptime_string[: uptime_string.rfind(":")]

        delta = str(datetime.datetime.now() - self.start_time)
        tmp_str = delta[: delta.rfind(":")]
        uptime_string += " (" + tmp_str + ")"
        return uptime_string

    def every_min(self):
        if self.is_access_exclusive():
            return
        uptime_string = self._get_uptime()

        if not self.__ip_ok:
            self.__get_ip_info()

        content = [
            "Uptime  : " + uptime_string,
            "IP-Addr : " + self.__ip_addr,
            "Hostname: " + self.__fqdn,
        ]
        self.set_content(content)

    def every_hour(self):
        self.__ip_ok = False

    def _exit_exclusive_access(self):
        super()._exit_exclusive_access()
        self.every_min()

    def exclusive_access(self, enter):
        super().exclusive_access(enter)
        if enter:
            self._set_new_size(
                0, 0, self._config.getint("size_x"), self._config.getint("size_y")
            )
            self._x_alignment = 0
            self._y_alignment = 0

            self.set_header("System Info")

            with open("/proc/uptime", encoding="utf8") as proc_uptime:
                uptime_seconds = int(float(proc_uptime.readline().split()[0]) / 60) * 60
                uptime_string = str(datetime.timedelta(seconds=uptime_seconds))
                uptime_string = uptime_string[: uptime_string.rfind(":")]
            delta = str(datetime.datetime.now() - self.start_time)
            tmp_str = delta[: delta.rfind(":")]
            uptime_string += " (" + tmp_str + ")"
            content = [
                "System   : " + platform.system(),
                "Version  : " + platform.version(),
                "Machine  : " + platform.machine(),
                "Proc.    : " + platform.processor(),
                "Platform : " + platform.platform(),
                "",
                "Uptime   : " + self._get_uptime(),
                "IP-Addr  : " + self.__ip_addr,
                "Hostname : " + self.__fqdn,
            ]
            self._max_lines = len(content)
            self.set_content(content)

            box_width, box_height = self.calc_or_draw_text(False)
            box_width += self._inner_offset * 2
            box_height += self._inner_offset * 2
            weight = self._config.getint("size_x")
            height = self._config.getint("size_y")
            self._set_new_size(
                (weight - box_width) / 2,
                (height - box_height) / 2,
                box_width,
                box_height,
            )
