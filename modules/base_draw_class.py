#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pygame


class BaseDrawClass:
    # pylint: disable=too-many-arguments,too-many-instance-attributes
    def __init__(self, config, event_types, x1_y1, x2_y2, inner_offset, frame):
        classname = self.__class__.__name__
        classname = config.convert_camelcase(classname)
        self.classname = "_".join(classname.split('_')[1:])
        self._event_types = event_types
        x_1, y_1 = x1_y1
        x_2, y_2 = x2_y2
        self._config = config
        self._inner_offset = inner_offset
        self._frame = frame
        if self._config.getbackimage():
            self._backgroundcolor = None
        else:
            self._backgroundcolor = config.getbackcolor()
        self._drawcolor = config.getdrawcolor()
        self._framecolor = config.getframecolor()
        self._set_new_size(x_1, y_1, x_2, y_2)
        self.__exclusive_access = False

    # pylint: disable=attribute-defined-outside-init
    def _set_new_size(self, x_1, y_1, x_2, y_2):
        self._x1 = x_1
        self._y1 = y_1
        self._x2 = x_1 + x_2 - 1
        self._y2 = y_1 + y_2 - 1
        self._x_size = x_2
        self._y_size = y_2
        self._x1_off = x_1 + self._inner_offset
        self._y1_off = y_1 + self._inner_offset
        self._x2_off = x_1 + x_2 - self._inner_offset
        self._y2_off = y_1 + y_2 - self._inner_offset
        self._x_off_size = self._x2_off - self._x1_off
        self._y_off_size = self._y2_off - self._y1_off

    def post_init(self):
        pass

    # pylint: disable=attribute-defined-outside-init
    def _enter_exclusive_access(self):
        if self.__exclusive_access:
            return
        self.__exclusive_access = True
        self._x1_excl = self._x1
        self._y1_excl = self._y1
        self._x2_excl = self._x2
        self._y2_excl = self._y2
        self._x_size_excl = self._x_size
        self._y_size_excl = self._y_size
        self._x1_off_excl = self._x1_off
        self._y1_off_excl = self._y1_off
        self._x2_off_excl = self._x2_off
        self._y2_off_excl = self._y2_off
        self._x_off_size_excl = self._x_off_size
        self._y_off_size_excl = self._y_off_size

    # pylint: disable=attribute-defined-outside-init
    def _exit_exclusive_access(self):
        if not self.__exclusive_access:
            return
        self.__exclusive_access = False
        self._x1 = self._x1_excl
        self._y1 = self._y1_excl
        self._x2 = self._x2_excl
        self._y2 = self._y2_excl
        self._x_size = self._x_size_excl
        self._y_size = self._y_size_excl
        self._x1_off = self._x1_off_excl
        self._y1_off = self._y1_off_excl
        self._x2_off = self._x2_off_excl
        self._y2_off = self._y2_off_excl
        self._x_off_size = self._x_off_size_excl
        self._y_off_size = self._y_off_size_excl

    def is_access_exclusive(self):
        return self.__exclusive_access

    def set_backgroundcolor(self, color, set_contrast=False):
        self._backgroundcolor = color
        if set_contrast:
            if ((color[0] + color[1] + color[2]) / 3) > 128:
                self._drawcolor = (0, 0, 0)
            else:
                self._drawcolor = (255, 255, 255)

    def _drawframe(self):
        window = self._config.getwindow()
        drawrect = (self._x1, self._y1, self._x_size, self._y_size)
        if self._backgroundcolor:
            pygame.draw.rect(window, self._backgroundcolor, drawrect)
        if self._frame & 1:
            pygame.draw.line(window, self._framecolor, (self._x1, self._y1), (self._x2, self._y1))
        if self._frame & 2:
            pygame.draw.line(window, self._framecolor, (self._x2, self._y1), (self._x2, self._y2))
        if self._frame & 4:
            pygame.draw.line(window, self._framecolor, (self._x2, self._y2), (self._x1, self._y2))
        if self._frame & 8:
            pygame.draw.line(window, self._framecolor, (self._x1, self._y2), (self._x1, self._y1))

    def draw(self):
        # draw frame
        self._drawframe()

    # pylint: disable=no-self-use
    def every_sec(self):
        return False

    def every_min(self):
        pass

    def every_hour(self):
        pass

    # the box name is empty for the main box and contains the dynamic box name for dynamic boxes
    def click_event(self, _click_pos, _box_name):
        if self.is_access_exclusive():
            pygame.event.post(pygame.event.Event(self._event_types[2], block='--NO--', receiver=self))
        else:
            pygame.event.post(pygame.event.Event(self._event_types[2], block='--YES--', receiver=self))

    def exclusive_access(self, enter):
        if enter:
            self._enter_exclusive_access()
        else:
            self._exit_exclusive_access()
