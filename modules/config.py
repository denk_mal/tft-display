#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import ast
import configparser
import logging
import re
import sys

from .common_data import WHITE_COLOR, BLACK_COLOR

FONT_NAME = "droidsansmono"
CONFIG_VERSION = 3


class Config:
    def __init__(self):
        logging.debug("initialize Config object...")
        self.__window = None
        self.__flip_window = False
        self.__drawcolor = WHITE_COLOR
        self.__backcolor = BLACK_COLOR
        self.__framecolor = None
        self.__backimage = None
        parser = argparse.ArgumentParser()
        parser.add_argument("-c",
                            "--configfile",
                            help="name of configfile",
                            type=argparse.FileType(),
                            required=True
                            )
        parser.add_argument("-b",
                            "--__brightness",
                            help="set __brightness of the display",
                            type=int,
                            choices=range(0, 256),
                            metavar="[0-255]"
                            )
        parser.add_argument("-v",
                            "--verbose",
                            action="count",
                            dest="verbose",
                            default=0,
                            help="Increase verbosity.")
        args = parser.parse_args()

        self.__config = configparser.RawConfigParser()
        if args.configfile:
            self.__config.read_file(args.configfile)
        logger = logging.getLogger()
        if not args.verbose:
            logger.setLevel('ERROR')
        elif args.verbose == 1:
            logger.setLevel('WARNING')
        elif args.verbose == 2:
            logger.setLevel('INFO')
        elif args.verbose >= 3:
            logger.setLevel('DEBUG')
        else:
            print("verbosity (%d) not valid", args.verbose)
        if not self.__config.has_option('global', 'version'):
            logging.fatal("version in config file missing.")
            sys.exit(254)
        version = self.getint('version')
        if version != CONFIG_VERSION:
            logging.warning("App needs config version %d but config file is of version %d", CONFIG_VERSION, version)
        if not self.__config.has_option('global', 'fontname'):
            self.__config.set('global', "fontname", FONT_NAME)
        param_dict = vars(args)
        for key, value in param_dict.items():
            if key != "configfile" and key != "verbose" and value:
                logging.debug("set config with (%s,%s)", key, value)
                self.__config.set('global', key, str(value))

    def setwindow(self, window):
        self.__window = window

    def getwindow(self):
        return self.__window

    def setdrawcolor(self, color):
        self.__drawcolor = color

    def getdrawcolor(self):
        return self.__drawcolor

    def setbackcolor(self, color):
        self.__backcolor = color

    def getbackcolor(self):
        return self.__backcolor

    def setframecolor(self, color):
        self.__framecolor = color

    def getframecolor(self):
        if self.__framecolor:
            return self.__framecolor
        return self.__drawcolor

    def setbackimage(self, image):
        self.__backimage = image

    def getbackimage(self):
        return self.__backimage

    def get(self, option, default="", section='global'):
        if self.__config.has_option(section, option):
            return self.__config.get(section, option)
        return default

    def getint(self, option, default=0, section='global'):
        if self.__config.has_option(section, option):
            return self.__config.getint(section, option)
        return default

    def getfloat(self, option, default=0.0, section='global'):
        if self.__config.has_option(section, option):
            return self.__config.getfloat(section, option)
        return default

    def getboolean(self, option, default, section='global'):
        if self.__config.has_option(section, option):
            return self.__config.getboolean(section, option)
        return default

    def getcolor(self, option, default=None, section='global'):
        if self.__config.has_option(section, option):
            try:
                return ast.literal_eval(self.__config.get(section, option))
            except SyntaxError:
                pass
        return default

    def getdict(self, option, default=None, section='global'):
        if self.__config.has_option(section, option):
            try:
                return ast.literal_eval(self.__config.get(section, option))
            except SyntaxError:
                pass
        return default

    # taken from
    # https://stackoverflow.com/questions/1175208/elegant-python-function-to-convert-camelcase-to-snake-case
    @staticmethod
    def convert_camelcase(input_str):
        regex = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', input_str)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', regex).lower()
