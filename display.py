#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import glob
import importlib
import locale
import logging
import os
import signal
import socket
import sys
import time

# set environment var to supress pygame banner output
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "True"
# pylint: disable=wrong-import-position
import pygame

# don't remove this import; It is needed by side loading the draw_modules
# pylint: disable=import-error,wildcard-import
# from modules import *
from modules.output_display import OutputTFT
from modules.dyn_textbox import DynTextBox
from modules.dyn_draw import DynDraw

from modules.config import Config

if sys.version < '3':
    print("Python 2 is not longer supported! will Exit now")
    sys.exit(255)

# some global default settings
SCREEN_X = 320
SCREEN_Y = 240

FULLPATH_SCRIPT = os.path.abspath(sys.argv[0])
BASEDIR_SCRIPT = os.path.dirname(FULLPATH_SCRIPT)
FILENAME_SCRIPT = os.path.basename(FULLPATH_SCRIPT)


# ----------------------------------------------------------------

# from https://stackoverflow.com/questions/497885/python-element-wise-tuple-operations-like-sum
# tuple(map(lambda x, y: x + y, tuple1, tuple2))

# from http://stackoverflow.com/questions/17035699/pygame-requires-keyboard-interrupt-to-init-display
def signal_handler(signal_param, _frame):
    msg = f"Signal: {signal_param}"
    print(msg)
    time.sleep(1)
    sys.exit(0)


# this is needed by side loading the modules
# based on
# https://www.geeksforgeeks.org/how-to-dynamically-load-modules-or-classes-in-python/

def init_class(py_file):
    modulename = Config.convert_camelcase(py_file)
    classname = ''.join(word.title() for word in modulename.split('_'))
    try:
        module = importlib.import_module(f"modules.{modulename}")
        obj = getattr(module, classname)
    except ModuleNotFoundError:
        logging.error("Module named '%s' not found!", modulename)
        return None
    except AttributeError:
        logging.error("Class named '%s' not found!", classname)
        return None
    return obj


# https://stackoverflow.com/questions/11361973/post-event-pygame?rq=1
# in python console:
# sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# sock.sendto(b"ALERT: new email", ("127.0.0.1", 15006))
# or from bash
# echo "ALERT: new email" | nc -q0 -u 127.0.0.1 15006
class UdpToPygame:  # pylint: disable=too-few-public-methods
    def __init__(self):
        udp_ip = "0.0.0.0"
        udp_port = 15006
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setblocking(False)
        self.sock.bind((udp_ip, udp_port))

    def update(self):
        try:
            data, _addr = self.sock.recvfrom(1024)
            event = pygame.event.Event(pygame.USEREVENT, {'data': data})
            pygame.event.post(event)
        except socket.error:
            pass


class MainWindow:  # pylint: disable=too-many-instance-attributes
    def __init__(self, config_param):
        logging.debug("MainWindow intialized.")
        self.__config = config_param
        self.__event_types = []
        self.__draw_objects = []
        self.__event_objects = []
        self.__click_objects = []
        self.__dynamic_boxes = {}
        self.__output_obj = None
        self.__initialdrawcnt = 3
        self.__exclusive_access = None
        new_locale = self.__config.get('locale', 'de_DE.UTF8')
        try:
            locale.setlocale(locale.LC_ALL, new_locale)
        except locale.Error:
            logging.warning("locale '%s' could not be set!", new_locale)

    def moduleinit(self):
        module_list = self.__config.getdict("modules", {})
        for name in module_list:
            logging.debug("try to initialize modul: %s", name)
            clazz = init_class(f"draw{name}")
            if clazz:
                val = module_list[name]
                events = clazz(self.__config, self.__event_types, *val)
                self.__draw_objects.append(events)
                bottom_right = tuple(map(lambda x, y: x + y, val[0], val[1]))
                click_dict = {'top_left': val[0], 'bottom_right': bottom_right, 'object': events, 'name': name.lower()}
                self.__click_objects.append(click_dict)
        event_list = self.__config.getdict("events", {})
        for filename in glob.glob1(os.path.join(BASEDIR_SCRIPT, "modules"), "event_*.py"):
            name = filename[:-3]
            name = ''.join(word.title() for word in name.split('_'))
            if name[5:] in event_list:
                logging.debug("try to initialize modul: %s", name)
                clazz = init_class(name)
                if clazz:
                    events = clazz(self.__config, self.__event_types)
                    self.__event_objects.append(events)

    def draw(self):
        self.__output_obj.draw()
        if self.__exclusive_access:
            self.__exclusive_access.draw()
        else:
            for obj in self.__draw_objects:
                obj.draw()
        self.__output_obj.update()

    def sec_event(self, event_type, _param):
        if self.__initialdrawcnt > 0:
            refresh_output = True
            self.__initialdrawcnt -= 1
        else:
            refresh_output = False
        for obj in self.__draw_objects:
            refresh_output |= obj.every_sec()
        for obj in self.__event_objects:
            refresh_output |= obj.every_sec()

        # update of tft-display
        if time.localtime().tm_sec == 0:
            pygame.event.post(pygame.event.Event(event_type + 1, {}))
        elif refresh_output:
            self.draw()

    def min_event(self, event_type, _param):
        for obj in self.__draw_objects:
            obj.every_min()
        for obj in self.__event_objects:
            obj.every_min()
        if time.localtime().tm_min == 0:
            pygame.event.post(pygame.event.Event(event_type + 1, {}))
        else:
            self.draw()

    def hour_event(self, _event_type, _param):
        for obj in self.__draw_objects:
            obj.every_hour()
        for obj in self.__event_objects:
            obj.every_hour()
        self.draw()

    def alert_event(self, _event_type, param):
        message = ""
        if param:
            message = param['text']
        if message:
            self.__output_obj.alert(message)
        else:
            self.__output_obj.alert_off()
            self.draw()

    def click_event(self, click_pos):
        # self.__output_obj.backlight_toggle()
        if self.__output_obj.is_alert_active():
            # turn off alert
            logging.info("click event received: turned alert off")
            self.__output_obj.alert_off()
        else:
            logging.info("click event for position (%d;%d)", click_pos[0], click_pos[1])
            if self.__exclusive_access:
                self.__exclusive_access.click_event(click_pos, 'exclusive_access')
            else:
                for click_dict in self.__click_objects:
                    top_left = click_dict.get('top_left')
                    bottom_right = click_dict.get('bottom_right')
                    if top_left[0] < click_pos[0] < bottom_right[0] and top_left[1] < click_pos[1] < bottom_right[1]:
                        rel_pos = tuple(map(lambda x, y: x - y, click_pos, top_left))
                        response_obj = click_dict.get('object')
                        logging.info("click event send to '%s'", type(response_obj).__name__)
                        response_obj.click_event(rel_pos, click_dict.get('name'))

    def dynamic_box(self, _event_type, param):
        if not param:
            return

        dynamicbox = None
        box_type = param['target_type']
        dyn_boxname = param['id']
        receiver = param['receiver']
        if box_type == 'textbox':
            content = param['data']
            header = ''
            if 'header' in param:
                header = param['header']
            if dyn_boxname not in self.__dynamic_boxes:
                val = self.__config.getdict(dyn_boxname, (), "dyn_text_box")
                if val:
                    dynamicbox = DynTextBox(self.__config, self.__event_types, *val)
                    if dynamicbox:
                        self.__dynamic_boxes[dyn_boxname] = dynamicbox
                        if header:
                            dynamicbox.set_header(header)
                        dynamicbox.set_content(content, True)
                        if dynamicbox not in self.__draw_objects:
                            self.__draw_objects.append(dynamicbox)
                            bottom_right = tuple(map(lambda x, y: x + y, val[0], val[1]))
                            click_dict = {'top_left': val[0], 'bottom_right': bottom_right, 'object': receiver,
                                          'name': dyn_boxname}
                            self.__click_objects.append(click_dict)
            else:
                dynamicbox = self.__dynamic_boxes[dyn_boxname]
                if header:
                    dynamicbox.set_header(header)
                if content:
                    dynamicbox.set_content(content)
        elif box_type == 'drawbox':
            if dyn_boxname not in self.__dynamic_boxes:
                val = self.__config.getdict(dyn_boxname, (), "dyn_drawbox")
                if val:
                    dynamicbox = DynDraw(self.__config, self.__event_types, *val)
                    if dynamicbox:
                        self.__dynamic_boxes[dyn_boxname] = dynamicbox
                        if dynamicbox not in self.__draw_objects:
                            self.__draw_objects.append(dynamicbox)
                            bottom_right = tuple(map(lambda x, y: x + y, val[0], val[1]))
                            click_dict = {'top_left': val[0], 'bottom_right': bottom_right, 'object': receiver,
                                          'name': dyn_boxname}
                            self.__click_objects.append(click_dict)
            else:
                dynamicbox = self.__dynamic_boxes[dyn_boxname]
        elif box_type == '_REMOVE_':
            if dyn_boxname in self.__dynamic_boxes:
                dynamicbox = self.__dynamic_boxes[dyn_boxname]
                self.__draw_objects.remove(dynamicbox)
                del self.__dynamic_boxes[dyn_boxname]
                new_click_dicts = []
                for click_dict in self.__click_objects:
                    if click_dict.get('name') != dyn_boxname:
                        new_click_dicts.append(click_dict)
                self.__click_objects = new_click_dicts

        if dynamicbox:
            if 'bg_color' in param:
                dynamicbox.set_backgroundcolor(param['bg_color'])
            else:
                dynamicbox.set_backgroundcolor(None)
            dynamicbox.draw()

    def exclusive_access(self, _event_type, param):
        message = ""
        if param:
            message = param['block']
        if self.__exclusive_access:
            self.__exclusive_access.exclusive_access(False)
            self.__exclusive_access = None
        if message == "--YES--":
            self.__exclusive_access = param['receiver']
            self.__exclusive_access.exclusive_access(True)
        self.draw()
        self.__output_obj.update()

    def main(self):
        logging.debug("Startup main() function")

        # init
        self.__output_obj = OutputTFT(self.__config)

        logging.debug("initialize pygame...")
        # avoid silly and dumb alsa errormessages
        # https://raspberrypi.stackexchange.com/questions/83254/pygame-and-alsa-lib-error
        os.environ['SDL_AUDIODRIVER'] = 'dsp'
        pygame.init()
        logging.debug("install signal handler...")
        signal.signal(signal.SIGTERM, signal_handler)
        signal.signal(signal.SIGINT, signal_handler)

        logging.debug("setup main window...")
        screen_x = self.__config.getint('size_x', SCREEN_X)
        screen_y = self.__config.getint('size_y', SCREEN_Y)
        if self.__config.getboolean('fullscreen', False):
            self.__config.setwindow(pygame.display.set_mode((screen_x, screen_y), pygame.FULLSCREEN))
            if self.__config.getboolean('hide_cursor', False):
                # pygame.mouse.set_visible(False)
                # workaround a bug in pygame; mouse pos is invalid if cursor is invisible!
                # so we made a real invisible cursor
                cursor = (0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00)
                mask = (0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00)
                pygame.mouse.set_cursor((8, 8), (0, 0), cursor, mask)
        else:
            self.__config.setwindow(pygame.display.set_mode((screen_x, screen_y)))
            pygame.mouse.set_visible(True)

        logging.debug("draw init screen now..")
        points = [(0, 0), (0, screen_y - 1), (screen_x - 1, 0), (screen_x - 1, screen_y - 1)]
        drawlist = [points[0], points[1], points[3], points[2], points[0],
                    points[3], points[1], points[2]]
        logging.debug("draw drawlist...")
        self.__output_obj.draw(drawlist)
        logging.debug("update display...")
        self.__output_obj.update()

        event_dict = {
            pygame.USEREVENT + 1: self.sec_event,
            pygame.USEREVENT + 2: self.min_event,
            pygame.USEREVENT + 3: self.hour_event,
            pygame.USEREVENT + 4: self.alert_event,
            pygame.USEREVENT + 5: self.dynamic_box,
            pygame.USEREVENT + 6: self.exclusive_access
        }

        logging.debug("init draw_modules...")
        self.__event_types = [pygame.USEREVENT + 4, pygame.USEREVENT + 5, pygame.USEREVENT + 6]
        self.moduleinit()

        logging.debug("create eventtimer...")
        pygame.event.set_allowed(None)
        pygame.event.set_allowed(pygame.QUIT)
        pygame.event.set_allowed(pygame.MOUSEBUTTONUP)
        pygame.event.set_allowed(pygame.USEREVENT)
        for event_type in event_dict:
            pygame.event.set_allowed(event_type)
        pygame.event.clear()

        # initialising has been finished; all modules are up now
        for obj in self.__draw_objects:
            obj.post_init()
        for obj in self.__event_objects:
            obj.post_init()

        for event_type, event in event_dict.items():
            event(event_type, {})

        logging.debug("set eventtimer...")
        pygame.time.set_timer(pygame.USEREVENT + 1, 1000)

        dispatcher = UdpToPygame()

        logging.debug("start main loop now")
        while True:
            event = pygame.event.wait()
            if event.type == pygame.MOUSEBUTTONUP:
                if event.button == 1:
                    self.click_event(pygame.mouse.get_pos())
            elif event.type == pygame.QUIT:
                pygame.quit()
                break
            elif event.type == pygame.USEREVENT:
                event_data = event.dict['data'].decode("utf-8").rstrip().split(':', 1)
                event_type = event_data[0]
                value = ""
                if len(event_data) > 1:
                    value = event_data[1]
                # - enter/exit fullscreen
                if event_type == "ALERT":
                    if value:
                        self.__output_obj.alert(value)
                    else:
                        self.__output_obj.alert_off()
                elif event_type == "ENTER_FS":
                    click_obj = None
                    for obj in self.__click_objects:
                        if obj['name'] == value.lower():
                            click_obj = obj['object']
                            break
                    if click_obj:
                        self.exclusive_access(None, {'block': '--YES--', 'receiver': click_obj})
                elif event_type == "EXIT_FS":
                    self.exclusive_access(None, {})
                elif event_type == "QUIT":
                    pygame.quit()
                    break
                elif event_type == "FORCE_RELOAD":
                    for obj in self.__event_objects:
                        obj.force_reload()
            elif event.type in event_dict:
                event_dict[event.type](event.type, event.dict)
            dispatcher.update()


if __name__ == "__main__":
    DATE_FORMAT = '%Y-%m-%d_%H:%M:%S'
    print("*" * 79)
    print(time.strftime(f"{DATE_FORMAT} Start of {os.path.basename(__file__)}"))
    # logging.basicConfig(filename='display.log',
    # format='%(asctime)s %(levelname)s:%(message)s',datefmt='%Y-%m-%d_%H:%M:%S')
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', datefmt=DATE_FORMAT)
    config = Config()
    try:
        logging.debug("try to get pid of own process")
        PID = str(os.getpid())
        pidfile_path = os.path.expanduser("~/.cache/run")
        try:
            os.makedirs(pidfile_path)
        except OSError:
            pass
        pidfile_path = os.path.join(pidfile_path, f"{FILENAME_SCRIPT}.pid")
        with open(pidfile_path, "w", encoding="utf8") as f:
            f.write(PID)
            f.close()
        logging.info("pid of own process = %s", PID)
    except IOError:
        logging.error("getting of pid failed!")

    mainwnd = MainWindow(config)
    mainwnd.main()
